#include <stdio.h>
#include <string.h>

 typedef enum { MASCULIN, FEMININ } Genre;

void genereLettre(Genre genre, const char* destinataire, const char* sujet,
		  unsigned int jour, unsigned int mois, const char* politesse,
		  const char* auteur)
{
  printf("Bonjour ");
  if (genre == MASCULIN)
    printf("cher");
  else
    printf("ch�re");
  printf(" %s,", destinataire);

  printf(
    "Je vous �cris � propos de %s.\n"                     
    "Il faudrait que nous nous voyons le %d/%d pour en discuter.\n"
    "Donnez-moi vite de vos nouvelles !\n"                         
    "%s, %s.\n"
    , sujet, jour, mois, politesse, auteur);
}

int main(void)
{
  genereLettre(FEMININ, "Mireille", "votre cours" , 18, 12, "Amicalement", 
	       "John");
  putchar('\n');
  genereLettre(MASCULIN, "John", "votre demande de rendez-vous", 16, 12,
	       "Sinc�rement", "Mireille");
  return 0;
}
