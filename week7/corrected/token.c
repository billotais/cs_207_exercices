#include <stdio.h>
#include <string.h>

/* La fonction suivante teste si le caract�re est un s�parateur
 *
 * �crire une fonction pr�sente l'avantage de pouvoir red�finir facilement 
 * la notion de s�parateur (et �ventuellement d'en d�finit plusieurs)
 */
int issep (char c) {
  return (c == ' '); /* retourne 1 si la condition est v�rifi�e, 0 sinon */
}

/* Il y a *plein* d'autres fa�ons d'�crire cette fonction.
 *
 * Je trouve celle-ci �l�gante.
 */
int nextToken(char const * str, int* from, int* len)
{
  const int taille = strlen(str); /* taille totale de la ligne entr�e */
  int i;

  /* D'abord, on saute tous les s�parateurs avant le premier 
   * mot � partir de from.
   * Notez que *from repr�sente la valeur point�e par from, 
   * c�d l'index qu'on a donn� en param�tre.
   */
  while ( (*from < taille) && issep(str[*from]) ) {
    ++(*from); /* on veut incr�menter la valeur point�e par from, pas son adresse! */
  }

  /* Maintenant, from pointe sur l'index de la premi�re lettre  
   * du premier mot qui nous int�resse. 
   * On avance jusqu'au prochain s�parateur ou la fin de str.
   */
  *len = 0;
  for (i = *from; ((i < taille) && !issep(str[i])); ++(*len), ++i);

  return (*len != 0);
}

/* ---------------------------------------------------------------------- */

/* On d�finit une TAILLE_MAX pour la phrase qui sera entr�e par
    l'utilisateur. */
#define TAILLE_MAX 1024

int main(void)
{
  char phrase[TAILLE_MAX+1];
  char mot[TAILLE_MAX+1];
  int debut = 0;
  int longueur = 0;
  int taille_lue;

  do {
    printf("Entrez une cha�ne : ");
    fgets(phrase, TAILLE_MAX, stdin);
    taille_lue = strlen(phrase) - 1;
    /* On supprime le "\n" lu, c�d le dernier caract�re 
     * (en le rempla�ant par "\0") 
     */
    if ((taille_lue >= 0) && (phrase[taille_lue] == '\n'))
      phrase[taille_lue] = '\0';
  } while ((taille_lue < 1) && !feof(stdin));

  printf("Les mots de \"%s\" sont :\n", phrase);
  while (nextToken(phrase, &debut, &longueur)) {
  
    /* On copie dans la variable mot les caract�res
     * de la phrase depuis l'index debut, 
     * de la taille longueur.
     */
    strncpy(mot, &(phrase[debut]), longueur);

    /* On rajoute un '\0' pour indiquer la fin du mot 
     * (sinon on ne peut pas employer %s).
     */
    mot[longueur] = '\0'; /* fin de mot */
    printf("'%s'\n", mot);
    debut += longueur;
  }
  return 0;
}
