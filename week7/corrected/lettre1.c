#include <stdio.h>

void genereLettre(void)
{
  printf( 
    "Bonjour ch�re Mireille,\n"                                    
    "Je vous �cris � propos de votre cours.\n"                     
    "Il faudrait que nous nous voyons le 18/12 pour en discuter.\n"
    "Donnez-moi vite de vos nouvelles !\n"                         
    "Amicalement, John.\n"
  );
}

int main(void)
{
  genereLettre();
  return 0;
}
