#include <stdio.h>
#include <math.h>


int main(void){

    int n;
    do {
        printf("Entrez un nombre n > 1 : ");
        scanf("%d", &n);
    } while (n <= 1);

    if (n % 2 == 0) {
        printf("Le nombre n'est pas premier car il est divisible par 2\n");
    }
    else {
        for (int i = 3; i <= sqrt(n); i += 2) {
            if (n % i == 0) {
                printf("Le nombre n'est pas premier car il est divisible par %d\n", i);
                return 0;
            }
        }
        printf("Le nombre est premier\n");

    }
    return 0;
}
