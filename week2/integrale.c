#include <stdio.h>
#include <math.h>

double demander_nombre (void) {
    double i;
    printf("Entrez un nombre : ");
    scanf("%lf", &i);
    return i;
}

double f (double x) {
    return pow(x, 2);
}

double integre (double a, double b) {
    return ((b-a)/840) * (41*f(a) +
                          216*f((5*a + b)/6) +
                          27*f((2*a + b)/3) +
                          272*f((a + b)/2) +
                          27*f((a + 2*b)/3) +
                          216*f((a + 5*b)/6) +
                          41*f(b));
}

int main (void) {
    double a = demander_nombre();
    double b = demander_nombre();
    printf("%lf\n", integre(a, b));
}