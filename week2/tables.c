#include <stdio.h>

int main(void){
    printf("Tables de multiplication\n\n");
    for (int i = 2; i <= 10; ++i){
        printf("Tables de %d\n", i);
        for (int j = 1; j <= 10; ++j){
            printf("    %d * %d = %d\n", j, i, i*j);
        }
        printf("\n");
    }
    return 0;
}