#include <stdio.h>
#include <math.h>

int main(void) {
    double x;
    printf("Entrez un réel : ");
    scanf("%lf", &x);

    if (x == 0) {
        
        printf("Expression indéfinie : 1\n");
    }
    else {
        printf("%lf\n", x/(1-exp(x)));
    }

    if (x <= 0 || x == 1) {
        printf("Expression indéfinie : 2\n");
    }
    else {
        printf("%lf\n", x*log(x)*exp(2/(x-1)));
    }

    if (x == 2 || (x*x - 8*x < 0)) {
        printf("Expression indéfinie : 3\n");
    }
    else {
        printf("%lf\n", (- x - sqrt(pow(x, 2) - 8*x))/(2-x));
    }

    if (x == 0 || pow(x, 2) - (1/x) <= 0 || (sin(x) - x/20)*(log(pow(x, 2) - (1/x))) < 0) {
        printf("Expression indéfinie : 4\n");
    }
    else {
        printf("%lf\n",  sqrt((sin(x) - x/20)*(log(pow(x, 2) - (1/x)))));
    }

    return 0;
}