#include <stdio.h>
#include <math.h>

int main(void){

    double h0, eps;
    int nbr;
    const double g = 9.81;

    printf("Entrez la valeur de H0 : ");
    scanf("%lf", &h0);
    do {
        printf("Entrez la valeur de eps : ");
        scanf("%lf", &eps);
    } while (eps < 0 || eps >= 1);
    do {
        printf("Entrez la valeur de NBR : ");
        scanf("%lf", &nbr);
    } while (nbr < 0);
    
    double h = h0;
    double v = sqrt(2*h*g);
    double v1, h1;
    for (int i = 0; i < nbr; ++i){
        v1 = eps*v;
        h1 = (v1*v1)/(2*g);
        v = v1;
        h = h1;
    }
    printf("Au %lf rebond, la hateur sera de %lf m\n", nbr, h);
    return 0;
}