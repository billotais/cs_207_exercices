#include <stdio.h>
#include <math.h>

double demander_nombre(void) {
    double x;
    printf("Entrez un nombre : ");
    scanf("%lf", &x);
    return x;
}
double factorielle(int k) {
    double fact = 1;
    for (int i = 1; i <= k; ++i) {
        fact *= i;
    }
    return fact;
}

double somme_partielle(double d, int N) {
    double sum = 0;
    for (int i = 0; i < N; ++i) {
        sum += (pow(-1, i)*pow(d, 2*i)) / factorielle(2*i);
    }
    return sum;
}

int main (void) {
    double n;
    printf("Entrez un nombre entier positif N : ");
    scanf("%lf", &n);

    double x;
    do {
        x = demander_nombre();
        printf("cos(%lf) = %.5f\n", x, somme_partielle(x, n));
    } while(x != 0);
   
   return 0;
}