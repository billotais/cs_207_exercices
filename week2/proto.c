#include <stdio.h>

int demander_nombre(int min, int max);

int main(void) {
    int min = 2;
    int max = 10;
    int n = demander_nombre(2, 10);
    printf("%d\n", n);
    return 0;
}

int demander_nombre(int min, int max) {
    int x;
    if (max <= min) {
        do {
            printf("Entrez un entier plus grand ou égal à %d : ", min);
            scanf("%d", &x);
        } while (x < min);   
    }
    do {
        printf("Entrez un entier entre %d et %d : ", min, max);
        scanf("%d", &x);
    } while (x < min || x > max);
    
    return x;
}
