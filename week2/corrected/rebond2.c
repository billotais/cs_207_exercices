#include <stdio.h>
#include <math.h>

double const g = 9.81;

int main(void)
{
    /* D�clarations */

    double v = 0.0, v1 = 0.0;                /* vitesses avant et apr�s le rebond */
    double h = 0.0, h1 = 0.0;                /* hauteur avant le rebond, hauteur de remont�e */
    double H0 = 1.0, eps = 0.1, h_fin = 0.1; /* entr�es de l'utilisateur */
    int nombre = 0;

    /*
     * Entr�e des valeurs par l'utilisateur,
     * avec test de validit�
     */

    do {
        printf("Coefficient de rebond (0 <= coeff < 1) :\n");
        scanf("%lf", &eps);
    } while ( (eps < 0.0) || (eps >= 1.0) );
    do {
        printf("Hauteur initiale      (0 <= H0)        :\n");
        scanf("%lf", &H0);
    } while ( H0 < 0.0 );

    do {
       printf("Hauteur finale  (0 <= h_fin ) :\n");
       scanf("%lf", &h_fin);
    } while ((h_fin < 0.0) || (h_fin > H0));

    /* Boucle de calcul */

    h = H0;
    nombre = 0;
    do {
        v  = sqrt(2.0 * g * h);
        v1 = eps * v;               /* vitesse apr�s le rebond */
        ++nombre;                   /* incr�mente le nombre de rebonds */
        h1 = (v1 * v1) / (2.0 * g); /* la hauteur � laquelle elle remonte... */
        h  = h1;                    /* ...qui devient la nouvelle hauteur initiale */

	/* nombre est incr�ment� plus haut, 
	 * il peut donc �tre affich� directement ici
	 * (par opposition � l'exercice pr�c�dent)
	 */
        printf("rebond %d : %f\n", nombre, h); 

    } while(h1 > h_fin);

    /* Affichage du r�sultat */

    printf("Nombre de rebonds : %d\n", nombre);
    return 0;
}
