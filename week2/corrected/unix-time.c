/*
 * unix-time.c
 * ANSI C89
 */

#include <stdio.h>
#include <time.h>

#define UNIX_EPOCH_YEAR 1970

/* copi� de zune.c */
typedef enum {
    JANUARY = 1,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER
} Month;

int IsLeapYear(int y)
{

    return (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0));
}

/* on a besoin de l'ann�e (year) pour le cas sp�cial (F�vrier) */
int DaysForMonth(int year, Month month)
{
    int days = 31;

    switch (month) {
    case FEBRUARY: /* cas sp�cial */
        if (IsLeapYear(year))
            days = 29;
        else
            days = 28;
        break;
    case APRIL:
    case JUNE:
    case SEPTEMBER:
    case NOVEMBER:
        days = 30;
        break;
    default:
        days = 31;
    }

    return days;
}

int main(void)
{
    time_t now = time(NULL);
    time_t seconds, minutes, hours;
    int year = UNIX_EPOCH_YEAR;
    time_t days = 1, d = 31;
    Month month = JANUARY;

    printf("%u secondes se sont ecoulees depuis le 1.1.1970 a minuit.\n", now);

    seconds = now % 60;
    now /= 60;
    minutes = now % 60;
    now /= 60;
    hours = now % 24;
    now /= 24;

    /* copi� de zune.c */
    /* calcul de year */
    days = now;
    year = UNIX_EPOCH_YEAR;
    while (days > 0) {
        if (IsLeapYear(year))
            days -= 366;
        else
            days -= 365;
        ++year;
    }
    --year;
    if (IsLeapYear(year))
        days += 366;
    else
        days += 365;

    /* calcul de month */
    month = JANUARY;
    d = DaysForMonth(year, month);
    while (days > d) {
        days -= d;
        d = DaysForMonth(year, ++month);
    }
    ++days; /* 0 corespond au 1er Janvier 1970, il faut faire +1 */

    printf("Nous sommes donc le %02d/%02d/%d a %02d:%02d:%02d\n",
            days, month, year, hours, minutes, seconds);

    return 0;
}
