// C99, pour changer
#include <stdio.h>
#include <math.h>

int main(void)
{
    // Saisie du nombre � tester
    int n = 2;
    do {
        printf("Entrez un nombre entier > 1 :\n");
        scanf("%d", &n);
    } while (n <= 1);

    int diviseur = 1;  // Diviseur trouv�. Si c'est 1, alors le nombre est premier

    if (0 == (n % 2)) {
        // Le nombre est pair
        if (n != 2) {
            diviseur = 2; // n n'est pas premier (2 est diviseur)
        }
    } else {
        const double borne_max = sqrt((double) n); 
        for (int i = 3; (diviseur == 1) && (i <= borne_max); i += 2) {
            if (0 == (n % i)) {
                diviseur = i; // n n'est pas premier (i est diviseur)
            }
        }
    }

    printf("%d", n);

    if (diviseur == 1) {
        printf(" est premier");
    } else {
        printf(" n'est pas premier, car il est divisible par %d", diviseur);
    }
    printf("\n");
    return 0;
}
