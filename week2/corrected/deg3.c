/* C89 */
#include <stdio.h>

/* ligne pour avoir M_PI (= pi). A mettre AVANT le include de math.h. */
#define  _USE_MATH_DEFINES
#include <math.h>
/* Si vraiment le compilateur respecte strictement le standard, m�me *
 * _USE_MATH_DEFINES ne fera pas l'affaire. On fait alors le travail *
 * nous m�me :                                                       */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

int main(void)
{
  double a0 = 0.0, a1 = 0.0, a2 = 0.0,
    z1 = 0.0, z2 = 0.0, z3 = 0.0,
    D = 0.0, Q = 0.0, R = 0.0, S = 0.0, T = 0.0;

  printf("Entrez a2, puis a1, puis a0 :\n");
  scanf("%lf %lf %lf", &a2, &a1, &a0);

  Q = (3.0 * a1 - a2*a2) / 9.0;
  R = (9.0 * a2 * a1 - 27.0 * a0 - 2.0 * a2*a2*a2) / 54.0;
  D = Q*Q*Q + R*R;
  printf("(pour info D = %f)\n", D);

  if (D < 0.0) { /* test du d�terminant */
     /* cas de trois racines r�elles */

     T  = acos( R / sqrt(-Q*Q*Q) );
     z1 = 2.0 * sqrt(-Q) * cos(T/3.0)              - a2/3.0;
     z2 = 2.0 * sqrt(-Q) * cos( (T+2*M_PI) / 3.0 ) - a2/3.0;
     z3 = 2.0 * sqrt(-Q) * cos( (T+4*M_PI) / 3.0 ) - a2/3.0;
     printf("Trois racines ( %f , %f , %f )\n", z1, z2, z3);  
  } else {
     /* cas de moins de trois racines r�elles */

     /* calcul de S */
     double s = R+sqrt(D);
     const double un_tiers = 1.0/3.0;
     if (0.0 == s)     { S = 0.0;                }
     else if (s < 0.0) { S = -pow(-s, un_tiers); }
     else if (s > 0.0) { S =  pow( s, un_tiers); }

     /* calcul de T */
     s = R-sqrt(D);
     if (0.0 == s)     { T = 0.0;                }
     else if (s < 0.0) { T = -pow(-s, un_tiers); }
     else if (s > 0.0) { T =  pow( s, un_tiers); }

     printf("(pour info S = %f, T = %f, S+T= %f)\n", S, T, S+T);

     /* calcul des solutions */
     z1 = -a2 / 3.0 + S + T;
     if ((0.0 == D) && (S+T != 0.0)) {
       z2 = -a2 / 3.0 - (S + T) / 2.0;
       printf("Deux racines...\n");
       printf("   l'une simple   : %f\n", z1);
       printf("   l'autre double : %f\n", z2);
     } else {
       printf("Une seule racine : %f\n", z1);
     }
  }

  return 0;
}
