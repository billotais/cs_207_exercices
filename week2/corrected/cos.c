#include <stdio.h>

int demander_nombre(int a, int b)
{
    int res = 0;

   do {
       printf("Entrez le degr� d'approximation entre %d et %d : ",
              a, b);
       scanf("%d", &res);
    } while ((res < a) || (res > b));
    return res;
}

double factorielle(int k)
{
   /* rappel: factorielle(0) = 1 et factorielle(1) = 1 */
   double fact = 1.0;
    int i;
    for(i = 2; i <= k; ++i) fact *= i; 
    return fact;
}

double somme_partielle(double x, int N)
{
  double current_approx = 0.0; /* approximation courante            */
  double powerx = 1.0;       /* puissance de x (initialis�e � x^0=1) */
    int i;

    for(i = 0; i < N; ++i)
    {
        if (i%2 == 0) {
            current_approx += powerx / factorielle(i*2);
        } else {
            current_approx -= powerx / factorielle(i*2);
        }
        powerx *= x*x; /* powerx is x^0, x^2, x^4, x^6, ...x^2n */
    }

    return current_approx;
}

int main(void)
{
    int N = demander_nombre(2, 170);
    double x = 0.0;

    do {
        printf("Entrez une valeur x pour le calcul de cos(x) : ");
        scanf("%lf", &x);
        printf("cos(x) ~ %.12f\n", somme_partielle(x,N));
    } while (x != 0.0);
    return 0;
}
