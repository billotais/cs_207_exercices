// C99, pour changer un peu
#include <stdio.h>

int main(void)
{
  printf("     Tables de multiplication\n");

  // On it�re sur les tables de 2 � 10 (compris)
  for (int i = 2; i <= 10; ++i) {
    printf("\n Table de %d :\n", i);    

    // Pour chaque table, on it�re de 1 � 10 (multiplicateurs)
    for (int j = 1; j <= 10; ++j) {

      // Affichage de la multiplication effectu�e et son r�sultat
      printf("    %d * %d = %d\n", i, j, i*j);

    }
  }

  return 0;
}
