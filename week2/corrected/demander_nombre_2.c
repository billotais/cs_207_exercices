#include <stdio.h>

int demander_nombre(int, int);

int main(void)
{
    printf("Le nombre entr� est : %d\n", demander_nombre(1, 100));
    return 0;
}

int demander_nombre(int a, int b)
{
    int res = 0;

   /* �change a et b si ils ne sont pas dans le bon ordre.
    * Ceci est N�CESSAIRE si on ne veut pas de boucle infinie
    * dans le cas ou on appelle la fonction avec a>b !!
    */
    if (a > b) { res=b; b=a; a=res; }

   do {
       printf("Entrez un nombre entier compris entre %d et %d :\n",
              a, b);
       scanf("%d", &res);
    } while ((res < a) || (res > b));
    return res;
}
