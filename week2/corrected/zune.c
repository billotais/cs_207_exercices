/*
 * zune.c
 * ANSI C89
 */

#include <stdio.h>
#include <stdlib.h> /* atoi(3) */

#define MICROSOFT_EPOCH_YEAR 1980
#define december_31_2008 10593

typedef enum {
    JANUARY = 1,
    FEBRUARY,
    MARCH,
    APRIL,
    MAY,
    JUNE,
    JULY,
    AUGUST,
    SEPTEMBER,
    OCTOBER,
    NOVEMBER,
    DECEMBER
} Month;

int IsLeapYear(int y)
{

    return (((y % 4 == 0) && (y % 100 != 0)) || (y % 400 == 0));
}

/* on a besoin de l'ann�e (year) pour le cas sp�cial (F�vrier) */
int DaysForMonth(int year, Month month)
{
    int days = 31;

    switch (month) {
    case FEBRUARY: /* cas sp�cial */
        if (IsLeapYear(year))
            days = 29;
        else
            days = 28;
        break;
    case APRIL:
    case JUNE:
    case SEPTEMBER:
    case NOVEMBER:
        days = 30;
        break;
    default:
        days = 31;
    }

    return days;
}

int main(void)
{
    int year = MICROSOFT_EPOCH_YEAR;
    int days = 1, d = 31;
    Month month = JANUARY;

    printf("Entrez le nombre de jours �coul�s depuis le 31/12/1979 : ");
    scanf("%d", &days);

    /* calcul de year */
    while (days > 0) {
        if (IsLeapYear(year))
            days -= 366;
        else
            days -= 365;
        ++year;
    }
    --year;
    if (IsLeapYear(year))
        days += 366;
    else
        days += 365;

    /* calcul de month */
    month = JANUARY;
    d = DaysForMonth(year, month);
    while (days > d) {
        days -= d;
        d = DaysForMonth(year, ++month);
    }

    printf("%02d/%02d/%d\n", days, month, year);

    return 0;
}
