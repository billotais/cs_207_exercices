#include <stdio.h>

int demander_nombre(int min, int max);

int main(void)
{
    printf("Le nombre entr� est : %d\n", demander_nombre(1, 100));
    printf("Le nombre entr� est : %d\n", demander_nombre(10, -1));
    return 0;
}

int demander_nombre(int a, int b)
{
    int res = 0;

    do {
      printf("Entrez un nombre entier ");
      if (a >= b)
	printf("sup�rieur ou �gal � %d", a);
      else
	printf("compris entre %d et %d", a, b);
      printf(" :\n");
      scanf("%d", &res);
    } while ((res < a) || ((a < b) && (res > b)));

   return res;
}
