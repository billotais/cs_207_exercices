/* C89 */
#include <stdio.h>

int main(void)
{
    double cumul = 0.0, S = 0.0, S0 = 0.0, r = 0.0, ir = 0.0;
    int nbr = 0;   /* nombre de remboursements */

    do {
        printf("Somme pr�t�e                    (S0 > 0) :\n");
        scanf("%lf", &S0);
    } while (S0 <= 0.0);

    do {
        printf("Montant fixe rembours� chaque mois (r > 0) :\n");
        scanf("%lf", &r);
    } while (r <= 0.0);

    do {
        printf("Taux d'int�r�t en %% (0 < tx < 100) :\n");
        scanf("%lf", &ir);
    } while ( (ir <= 0) || (ir >= 100) );
    ir /= 100.00;  /* le taux d'int�r�t a �t� donn� en %, nous le transformons */

    S = S0;        /* initialisation de la somme r�siduelle � la somme initiale */
    while (S > 0.0) {
        ++nbr;     /* on incr�mente le nombre de mois requis */
        cumul = cumul + ir * S;
        S = S - r; /* on d�cr�mente la somme r�siduelle */
        printf("%d: S=%f, cumul=%f\n", nbr, S, cumul); /* pour info, non demand� */
    }

    /* la valeur %.2f repr�sente un r�el avec 2 chiffres apr�s la virgule */
    printf("Somme des int�r�ts encaiss�s : %.2f (sur %d mois).\n", cumul, nbr);

    return 0;
}
