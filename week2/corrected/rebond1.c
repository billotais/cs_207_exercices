/* C89 */
#include <stdio.h>
#include <math.h>

/* On d�clare g constante. Elle ne peut plus �tre modifi�e dans le   *
 * reste du code                                                     */
double const g = 9.81;       

int main(void)
{
    /* D�clarations */

    double v = 0.0, v1 = 0.0;   /* vitesses avant et apr�s le rebond */
    double h = 0.0, h1 = 0.0;   /* hauteur avant le rebond, hauteur de remont�e */
    double H0 = 1.0, eps = 0.1; /* hauteur initiale, coefficient de rebond */
    int nombre = 0, NBR = 1;

    /*
     * Entr�e des valeurs par l'utilisateur,
     * avec test de validit�
     */

    do {
        printf("Coefficient de rebond (0 <= coeff < 1) :\n");
        scanf("%lf", &eps);

    /* r�p�tition tant que l'utilisateur n'entre pas une valeur
     * correcte pour eps                                        */
    } while ( (eps < 0.0) || (eps >= 1.0) );

    do {
        printf("Hauteur initiale      (0 <= H0)        :\n");
        scanf("%lf", &H0);

    /* r�p�tition tant que l'utilisateur n'entre pas une valeur
     * positive pour H0                                         */
    } while ( H0 < 0.0);

    do {
        printf("Nombre de rebonds     (0 <= N )        :\n");
        scanf("%d", &NBR);

    /* r�p�tition tant que l'utilisateur n'entre pas une valeur
     * positive pour NBR                                        */
    } while (NBR < 0);

    /* ========== Boucle de calcul ========== */

    /* Au d�part (0 rebond), la hauteur de rebond vaut H0 */
    h = H0;

    for (nombre = 0; nombre < NBR; ++nombre) {
        v  = sqrt(2.0 * g * h);
        v1 = eps * v;              /* vitesse apr�s le rebond */
        h1 =(v1 * v1) / (2 * g);   /* la hauteur � laquelle elle remonte... */
        h  = h1;                   /* ...qui devient la nouvelle hauteur initiale */

        printf("rebond %d : %f\n", nombre+1, h);
    }

    /* Affichage du r�sultat */
    printf("Au %d�me rebond, la hauteur sera de %f m.\n", NBR, h);

    return 0;
}
