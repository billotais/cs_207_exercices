#include <stdio.h>
#include <math.h>

int main(void){

    double s0, r, ir;
    
    do {
        printf("Entrez la valeur de S0 : ");
        scanf("%lf", &s0);
    } while (s0 < 0);
    do {
        printf("Entrez la valeur de r : ");
        scanf("%lf", &r);
    } while (r <= 0);
    do {
        printf("Entrez la valeur de ir : ");
        scanf("%lf", &ir);
    } while (ir < 0 || ir >= 1);
    
    double sum = 0;
    int mois = 0;
    while (s0 > 0) {
        
        sum += ir*s0;
        s0 -= r;
        ++mois;
    }
   
    printf("Somme des intérêts encaissés : %.2f (en 24 mois)\n", sum);
    return 0;
}