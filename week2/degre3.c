#include <stdio.h>
#include <math.h>

int main(void) {
    double const pi = 3.1415;
    double a0, a1, a2;
    double z1, z2, z3;

    printf("Entrez la valeur de a0 : ");
    scanf("%lf", &a0);
    printf("Entrez la valeur de a1 : ");
    scanf("%lf", &a1);
    printf("Entrez la valeur de a2 : ");
    scanf("%lf", &a2);

    double q = (3*a1 - pow(a2, 2)) / 9;
    double r = (9*a1*a2 - 27*a0 - 2*pow(a2, 3)) / 54;
    double d = pow(q, 3) + pow(r, 2);
    


    if (d < 0) {
        double theta = acos(r/sqrt(-pow(q, 3)));
        z1 = 2*sqrt(-q)*cos(theta/3.0) - (1.0/3.0)*a2;
        z2 = 2*sqrt(-q)*cos((theta + 2*pi)/3.0) - (1.0/3.0)*a2;
        z3 = 2*sqrt(-q)*cos((theta + 3*pi)/3.0) - (1.0/3.0)*a2;
        printf("Les solutions sont : z1 = %lf, z2 = %lf, z3 = %lf\n", z1, z2, z3);
    }
    else {
        
        double s = pow(r + sqrt(d),1.0/3.0);
        double t;
        if (r - sqrt(d) < 0){
            t = - (pow(-(r-sqrt(d)), 1.0/3.0));
        }
        else {
            t = pow(r - sqrt(d),1.0/3.0);
        }
      

        if (d == 0 && (s + t) != 0) {
            z1 = -(1.0/3.0)*a2 + s + t;
            z2 = -(1.0/3.0)*a2 - (1.0/2.0)*(s + t);
            printf("Les solutions sont : z1 = %lf, z2 = %lf\n", z1, z2);
        }
        else {
            z1 = -(1.0/3.0)*a2 + s + t;
            printf("La solution est : z1 = %lf\n", z1);
        }
    }

    return 0;
}