#include <stdio.h>
#include <math.h>

int main(void){

    double h0, eps, h_fin;
    
    const double g = 9.81;

    printf("Entrez la valeur de H0 : ");
    scanf("%lf", &h0);
    do {
        printf("Entrez la valeur de eps : ");
        scanf("%lf", &eps);
    } while (eps < 0 || eps >= 1);
    do {
        printf("Entrez la valeur de h_fin : ");
        scanf("%lf", &h_fin);
    } while (h_fin <= 0 || h_fin >= h0);
    
    double h = h0;
    double v = sqrt(2*h*g);
    int c = 0;
    do {
        v = eps*v;
        h = (v*v)/(2*g);
        ++c;
    } while (h > h_fin);
    
    printf("Nombre de rebons : %d\n", c);
    return 0;
}