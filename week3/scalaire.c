
#include <stdio.h>
double scalaire(const double u[], const double v[], size_t taille) {
  double total = 0;
  for (int i = 0; i < taille; ++i) {
    total += u[i]*v[i];
  }
  return total;
}

int main(void) {
  int N_MAX = 10;
  double v1[N_MAX];
  double v2[N_MAX];
  int n;

  printf("ENtrez la valeur de n : ");
  do {
    scanf("%d", &n);
  } while(n < 1 || n > N_MAX);

  for (int i = 0; i < n; ++i) {
    printf("v1_%d : ", i);
    scanf("%lf", &v1[i]);
  }
  for (int j = 0; j < n; ++j) {
    printf("v2_%d : ", j);
    scanf("%lf", &v2[j]);
  }
  double result = scalaire(v1, v2, n);
  printf("Result : %f\n", result);
  return 0;
}
