/* C89 */
#include <stdio.h>

#define N 10

typedef struct {
   double m[N][N];
   size_t lignes;
   size_t colonnes;
} Matrice;

Matrice lire_matrice(void);
void affiche_matrice(const Matrice);
Matrice multiplication(const Matrice a, const Matrice b);

/* ---------------------------------------------------------------------- */
int main(void)
{
  Matrice M1 = lire_matrice();
  Matrice M2 = lire_matrice();

  if (M1.colonnes != M2.lignes) 
    printf("Multiplication de matrices impossible !\n");
  else {
    printf("R�sultat :\n");
    affiche_matrice(multiplication(M1, M2));
  }
  return 0;
}

/* ---------------------------------------------------------------------- */
Matrice lire_matrice(void)
{
  Matrice resultat;
  size_t lignes = 2;
  size_t colonnes = 2;

  printf("Saisie d'une matrice :\n");

  do {
    printf("  Nombre de lignes (< %d) : ", N+1);
    scanf("%lu", &lignes); /* "%zu" en C99 ; c'est mieux. */
  } while ((lignes < 1) || (lignes > N));

  do {
    printf("  Nombre de colonnes (< %d) : ", N+1);
    scanf("%lu", &colonnes);
  } while ((colonnes < 1)  || (colonnes > N));

 resultat.lignes = lignes;
 resultat.colonnes = colonnes;
 { size_t i, j;
  for (i = 0; i < lignes; ++i) 
    for (j = 0; j < colonnes; ++j) {
      printf("  M[%lu, %lu]=", i+1, j+1);
      scanf("%lf", &resultat.m[i][j]);
    }
  }

  return resultat;
}

/* ---------------------------------------------------------------------- */
Matrice multiplication(const Matrice a, const Matrice b)
{
  Matrice resultat = a; /* Disons que par convention on retourne a si la
                         * multiplication ne peut pas se faire.
                         */
  size_t i, j, k; /* variables de boucle */

  if (a.colonnes == b.lignes) {
    resultat.lignes = a.lignes;
    resultat.colonnes = b.colonnes;

    for (i = 0; i < a.lignes; ++i) 
      for (j = 0; j < b.colonnes; ++j) {
        resultat.m[i][j] = 0.0;
        for (k = 0; k < b.lignes; ++k) 
	  resultat.m[i][j] += a.m[i][k] * b.m[k][j];
      }
  }

  return resultat;
}

/* ---------------------------------------------------------------------- */
void affiche_matrice(const Matrice matrice)
{
  size_t i, j;
  for (i = 0; i < matrice.lignes; ++i) {
    for (j = 0; j < matrice.colonnes; ++j) 
      printf("%g ", matrice.m[i][j]);
    putchar('\n');
  }
}
