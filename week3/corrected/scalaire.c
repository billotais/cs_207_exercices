// C99
#include <stdio.h>

double scalaire(const double u[], const double v[], size_t taille);

int main(void)
{
  size_t const N_MAX = 10;
  double v1[N_MAX];
  double v2[N_MAX];
  
  size_t n = 2;

  do {
    printf("Quelle taille pour vos vecteurs [1 � %zu] ?\n", N_MAX);
    scanf("%zu", &n);
  } while ((n < 1) || (n > N_MAX));

  printf("Saisie du premier vecteur :\n");
  for (size_t i = 0; i < n; ++i) {
    printf(" v1[%zu] = ", i);
    scanf("%lf", &v1[i]);
  }

  printf("Saisie du second vecteur :\n");
  for (size_t i = 0; i < n; ++i) {
    printf(" v2[%zu] = ", i);
    scanf("%lf", &v2[i]);
  }

  printf("le produit scalaire de v1 par v2 vaut %f\n",
         scalaire(v1, v2, n));
  return 0;
}

double scalaire(const double u[], const double v[], size_t taille)
{
  double somme = 0.0;
  for (size_t i = 0; i < taille; ++i) {
    somme += u[i] * v[i];
  }

  return somme;
}
