// C99
#include <stdio.h>
#include <math.h>

// ----------------------------------------------------------------------
typedef struct {
  double x;
  double y;
} Complexe;

typedef struct {
  Complexe z1;
  Complexe z2;
} Solutions;

// ----------------------------------------------------------------------
void affiche(const Complexe z);

Complexe addition      (const Complexe z1, const Complexe z2);
Complexe soustraction  (const Complexe z1, const Complexe z2);
Complexe multiplication(const Complexe z1, const Complexe z2);
Complexe division      (const Complexe z1, const Complexe z2);
Complexe racine        (const Complexe z);

Solutions resoudre_second_degre(const Complexe b, const Complexe c);

// ======================================================================
int main(void)
{
  Complexe b  = { 3.0, -2.0 };
  Complexe c  = { -5.0, 1.0 };

  Solutions s = resoudre_second_degre(b, c);

  printf("Avec b="); affiche(b);
  printf(" et c=");  affiche(c); 
  printf(" on a :\n");
  printf("  z1="); affiche(s.z1); putchar('\n');
  printf("  z2="); affiche(s.z2); putchar('\n');

  return 0;
}

// ======================================================================
void affiche(const Complexe z)
{
  if ((z.x == 0.0) && (z.y == 0.0)) {
    printf("0");
    return;
  }

  double y_affiche = z.y;
  
  if (z.x != 0.0) {
    printf("%g", z.x);
    if (z.y > 0.0)
      putchar('+');
    else if (z.y < 0.0) {
      putchar('-');
      y_affiche = -z.y;
    }
  }
  if (y_affiche != 0.0) {
    if ((z.x == 0.0) && (y_affiche == -1.0))
      putchar('-');
    else if (y_affiche != 1.0)
      printf("%g", y_affiche);
    putchar('i');
  }
}

// ======================================================================
Complexe addition(const Complexe z1, const Complexe z2)
{
  return (Complexe) { z1.x + z2.x, z1.y + z2.y };
}

// ======================================================================
Complexe soustraction(const Complexe z1, const Complexe z2)
{
  return (Complexe) { z1.x - z2.x, z1.y - z2.y };
}

// ======================================================================
Complexe multiplication(const Complexe z1, const Complexe z2)
{
  return (Complexe) {
    z1.x * z2.x - z1.y * z2.y ,
    z1.x * z2.y + z1.y * z2.x
  };
}

// ======================================================================
Complexe division(const Complexe z1, const Complexe z2)
{
  const double r = z2.x*z2.x + z2.y*z2.y;
  return (Complexe) {
    (z1.x * z2.x + z1.y * z2.y) / r ,
    (z1.y * z2.x - z1.x * z2.y) / r
  };
}

// ======================================================================
Complexe racine(const Complexe z)
{
  const double r = sqrt(z.x * z.x + z.y * z.y);
  Complexe retour;

  retour.x = sqrt((r + z.x) / 2.0);
  if (z.y >= 0.0)
    retour.y = sqrt((r - z.x) / 2.0);
  else
    retour.y = - sqrt((r - z.x) / 2.0);

  return retour;
}

// ======================================================================
Solutions resoudre_second_degre(const Complexe b, const Complexe c)
{
  // Pour faciliter l'�criture
  const Complexe deux   = { 2.0, 0.0 };
  const Complexe quatre = { 4.0, 0.0 };

  // delta^2 = b^2 - 4c
  const Complexe sdelta = racine(soustraction(multiplication(b, b),
                                              multiplication(quatre, c)));

  // Calcule -b (ou alors faire une fonction "oppose")
  const Complexe mb = { -b.x, -b.y };

  // R�ponse = -b +- delta / 2
  return (Solutions) {
    division( soustraction(mb, sdelta) , deux) ,
    division( addition    (mb, sdelta) , deux)
  };
}
