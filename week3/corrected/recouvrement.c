#include <stdio.h>

#define DIM 10

int const true = 1;
int const false = 0;
char const VIDE = '.';
char const PLEIN = '#';

typedef char Grille[DIM][DIM];

/* ---------------------------------------------------------------------- */
int remplitGrille(Grille grille, size_t ligne, size_t colonne,
                  char direction, size_t longueur)
{
  /* direction de l'objet sur x et y (vaut -1, 0, ou 1) */
  int dx, dy;

  /* les coordonnee de la case � modifier */
  size_t i = ligne;
  size_t j = colonne;

  /* la longueur modifi�e */
  size_t l;             

  /* est-ce possible de mettre tout l'�lement ? */
  int possible = true;

  switch (direction) {
  case 'N': dx = -1; dy =  0; break;
  case 'S': dx =  1; dy =  0; break;
  case 'E': dx =  0; dy =  1; break;
  case 'O': dx =  0; dy = -1; break;
  }

  /* avant de modifier la grille il faut v�rifier si c'est possible
   * de placer l'objet.
   */
  for (l = 0; 
       (possible) && (i < DIM) && (j < DIM) && (l < longueur); 
       ++l, i += dx, j += dy)
    if (grille[i][j] != VIDE) /* cette case est d�j� occup�e */
      possible = false;  /* on ne peut donc pas mettre l'objet voulu */

  /* Si l == longueur c'est que j'ai pu placer l'objet sur toute sa longueur.
   * Il se pourrait en effet que je sois sorti de la boucle ci-dessus parce que
   *  i >= DIM ou j >= DIM... ...ce qui n'a pas modifi� possible jusqu'ici
   */
  possible = possible && (l == longueur); 

  if (possible)
    /* on met effectivement l'objet, plus besoin de test ici */
    for (l = 0, i = ligne, j = colonne; l < longueur; 
         ++l, i += dx, j += dy)
      grille[i][j] = PLEIN;

  return possible;
}

/* ---------------------------------------------------------------------- */
void initGrille(Grille grille) 
{
  size_t i, j;
  for (i = 0; i < DIM; ++i) 
    for (j = 0; j < DIM; ++j) 
      grille[i][j] = VIDE;
}

/* ---------------------------------------------------------------------- */
void ajouterElements(Grille grille)
{
  int x = 1, y = 1;
  char dir = 'S';
  size_t l = 1;

  do {
    printf("Entrez coord. x : ");
    scanf("%d", &x);

    if (x >= 0) {
      printf("Entrez coord. y : ");
      scanf("%d", &y);

      if (y >= 0) {
        getchar(); /* absorbe le \n qui traine */
        do {
          printf("Entrez direction (N,S,E,O) : ");
          scanf("%c", &dir);
        } while ((dir != 'N') && (dir != 'S') && (dir != 'E') && (dir != 'O'));
        
        printf("Entrez taille : ");
        scanf("%lu", &l);
        
        printf("Placement en (%d,%d) direction %c longueur %lu -> ",
               x, y, dir, l);

        if (remplitGrille(grille, x, y, dir, l))
          printf("succ�s");
        else
          printf("ECHEC");
        putchar('\n');
        
      }
    }
  } while ((x >= 0) && (y >= 0));
}

/* ---------------------------------------------------------------------- */
void afficheGrille(const Grille grille)
{
  size_t i, j;
  for (i = 0; i < DIM; ++i) {
    for (j = 0; j < DIM; ++j)
      printf("%c", grille[i][j]);
    putchar('\n');
  }
}

/* ---------------------------------------------------------------------- */
int main(void)
{
  Grille grille;

  initGrille(grille);
  ajouterElements(grille);
  afficheGrille(grille);
  return 0;
}
