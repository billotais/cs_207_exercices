#include <stdio.h>

typedef struct {
  double x;
  double y;
} Complexe;

/* Solution simple */
void affiche(const Complexe z) 
{
  printf("(%g,%g)", z.x, z.y);

  /* autre solution :  printf("%g+%gi", z.x, z.y); */
}

/* Solution plus complexe mais plus �l�gante */
void affiche2(const Complexe z)
{
  double y_affiche = z.y;
  
  if ((z.x == 0.0) && (z.y == 0.0)) {
    printf("0");
    return;
  }

  if (z.x != 0.0) {
    printf("%g", z.x);
    if (z.y > 0.0)
      printf("+"); /* ou putchar('+'); */
    else if (z.y < 0.0) {
      putchar('-');
      y_affiche = -z.y;
    }
  }
  if (y_affiche != 0.0) {
    if ((z.x == 0.0) && (y_affiche == -1.0))
      putchar('-');
    else if (y_affiche != 1.0)
      printf("%g", y_affiche);
    putchar('i');
  }
}

/* ------------------------------------- */
Complexe addition(const Complexe z1, const Complexe z2)
{
  Complexe z;
  z.x = z1.x + z2.x;
  z.y = z1.y + z2.y;
  return z;
}

/* ------------------------------------- */
Complexe soustraction(const Complexe z1, const Complexe z2)
{
  Complexe z;
  z.x = z1.x - z2.x;
  z.y = z1.y - z2.y;
  return z;
}

/* ------------------------------------- */
Complexe multiplication(const Complexe z1, const Complexe z2)
{
  Complexe z;
  z.x = z1.x * z2.x - z1.y * z2.y;
  z.y = z1.x * z2.y + z1.y * z2.x;
  return z;
}

/* ------------------------------------- */
Complexe division(const Complexe z1, const Complexe z2)
{
  const double r = z2.x*z2.x + z2.y*z2.y;
  Complexe z;
  z.x = (z1.x * z2.x + z1.y * z2.y) / r;
  z.y = (z1.y * z2.x - z1.x * z2.y) / r;
  return z;
}

/* ------------------------------------- */
int main(void)
{
  Complexe un = { 1.0, 0.0 };
  Complexe i  = { 0.0, 1.0 };
  Complexe j;
  Complexe z;
  Complexe z2;

  j = addition(un, i);

  affiche(un); printf(" + "); affiche(i); printf(" = ");
  affiche(j); putchar('\n');

  z = multiplication(i,i);
  affiche(i); printf(" * "); affiche(i); printf(" = ");
  affiche(z); putchar('\n');

  z = multiplication(j,j);
  affiche(j); printf(" * "); affiche(j); printf(" = ");
  affiche(z); putchar('\n');

  z2 = division(z,i);
  affiche(z); printf(" / "); affiche(i); printf(" = ");
  affiche(z2); putchar('\n');

  z.x = 2.0; z.y = -3.0;
  z2 = division(z,j);
  affiche(z); printf(" / "); affiche(j); printf(" = ");
  affiche(z2); putchar('\n');

  return 0;
}
