#include <stdio.h>

typedef struct {
  double x;
  double y;
} complex;
void affiche(complex z) {
  printf("z = %0.2f + %0.2fi\n", z.x, z.y);
}
complex addition(complex z1, complex z2) {
  complex rslt = {z1.x + z2.x, z1.y + z2.y};
  return rslt;
}
complex soustraction(complex z1, complex z2) {
  complex rslt = {z1.x - z2.x, z1.y - z2.y};
  return rslt;
}
complex multiplication(complex z1, complex z2) {
  complex rslt = {z1.x * z2.x - z1.y * z2.y, z1.x * z2.y - z1.y * z2.x};
  return rslt;
}
int main(void) {
  complex z = {2.0, 3.5};
  affiche(z);
  return 0;
}
