#include <stdio.h>

#define N 10

typedef struct {

  unsigned int columns;
  unsigned int rows;
  double tab[N][N];

} Matrice;

Matrice lire_matrice(void);
Matrice multiplication(const Matrice a, const Matrice b);
void affiche_matrice(const Matrice m);

int main(void) {

  Matrice M1 = lire_matrice();
  Matrice M2 = lire_matrice();



  if (M2.rows != M1.columns) {
    printf("Multiplication de Matrices impossible\n");
    return 0;
  }

  Matrice result = multiplication(M1, M2);
  affiche_matrice(result);
  return 0;
}

Matrice lire_matrice(void) {
  int l, c;
  printf("Saisie d'une matrice : \n");
  printf("Nombre de lignes : ");
  scanf("%d", &l);
  printf("Nombre de colonnes : ");
  scanf("%d", &c);

  Matrice M = {c, l};

  for (int i = 0; i < l; i++) {
    for (int j = 0; j < c; j++) {
      double val;
      printf("M[%d, %d] : ", i + 1, j + 1);
      scanf("%lf", &M.tab[i][j]);

    }
  }


  return M;
}

Matrice multiplication(const Matrice a, const Matrice b) {

  int lFinal = a.rows;
  int cFinal = b.columns;
  Matrice final = {cFinal, lFinal};
  for (int i = 0; i < lFinal; ++i) {
    for (int j = 0; j < cFinal; ++j) {
      double tot = 0;
      for (int k = 0; k < a.columns; ++k){
        tot += a.tab[i][k]*b.tab[k][i];
      }
      final.tab[i][j] = tot;
    }
  }
  return final;
}

void affiche_matrice(const Matrice m) {
  printf("Resultat\n");
  for (int i = 0; i < m.rows; ++i) {
    printf("[");
    for (int j = 0; j < m.columns; ++j) {
      printf("%lf ", m.tab[i][j]);
    }
    printf("]\n");
  }
}
