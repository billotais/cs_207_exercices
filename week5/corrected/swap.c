void echange(int* a, int* b)
{
  /* on sauvegarde la valeur point�e par a pour ne pas la perdre */
  int const copie = *a; 

  /* la valeur point�e par a prend la valeur point�e par b       */
  *a = *b;

  /* la valeur point�e par b prend la valeur de copie            */
  *b = copie;
}
