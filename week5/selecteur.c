#include <stdio.h>

int main(void) {
  double valeur1 = 2.50;
  double valeur2 = -4.0;
  double valeur3 = 10.1;
  double* choix = NULL;

  int n;
  do {
      printf("Entrez un nombre n = 1, 2, 3 : ");
      scanf("%d", &n);
    } while (n < 1 || n > 3);

  if (n == 1) {choix = &valeur1;}
  if (n == 2) {choix = &valeur2;}
  if (n == 3) {choix = &valeur3;}

  printf("Vou avez choisi : %.2f\n", *choix);
  return 0;
}
