#include <stdio.h>

typedef unsigned char octet;


void afficher_octet(octet o) {
  int tab[8];
  int index = 0;
  while(o != 0){
    tab[index] = o % 2;
    o /= 2;
    index += 1;
  }

  for (int i = index-1  ; i >= 0; --i) {
    printf("%d", tab[i]);
  }
}

void affiche(size_t size, octet o) {
  printf("%02zu : ", size);
  afficher_octet(o);
  printf(" %3u ", o);
  if (o >= 32 || o <= 126) {
    printf("('%c')", o);
  }
  printf("\n");
}

void dump_mem(octet* ptr, size_t N) {
  printf("A partir de %p : ", ptr);
  for (int i = 0; i < N; ++i) {
    affiche(i, ptr[i]);
  }
}
int main(void) {
  int a = 80;
  int b = -80;
  double c = 0.5;
  double d = 0.1;

  octet* ptr = &a;

  dump_mem(&ptr, 4);
  ptr = &b;
  dump_mem(&b, 4);
  ptr = &c;
  dump_mem(&c, 4);
  ptr = &d;
  dump_mem(&d, 4);

  return 0;
}
