#include <stdlib.h>
#define PAGE_SIZE 128

typedef int* elem;
typedef struct {
  size_t size;
  size_t allocated;
  elem* data;
} tab;

tab* new_array(tab* array) {

  tab* result = array;
  if (result != NULL) {
    result->data = calloc(PAGE_SIZE, sizeof(elem));
    if (result->data != NULL) {
      result->size = 0;
      result->allocated = PAGE_SIZE;
    }
    else {
      result = NULL;
    }
  }
  return result;
}

tab* increase_size(tab* array) {
  tab* result = array;
  if (result != NULL) {
    elem* const copy = result->data;
    result->allocated += PAGE_SIZE;
    if ((result->allocated > PAGE_SIZE / sizeof(elem)) ||
      (result->data = realloc(result->data, result->allocated*sizeof(elem))) == NULL) {
        result->data = copy;
        result->allocated -= PAGE_SIZE;
        result = NULL;
      }
  }
  return result;
}

size_t add_element(elem e, tab* array) {

  if (array != NULL) {
    while(array->size > array->allocated) {
      if (increase_size(array) == NULL) {
        return 0;
      }
    }
    array->data[array->size] = e;
    array->size += 1;
    return array->size;
  }
  return 0;
}

size_t edit_elem(size_t i, elem e, tab* array) {
  if (array != NULL) {
    array->data[i] = e;
    return i;
  }
  return 0;
}

void delete_array(tab* array) {
  if (array != NULL && array->data != NULL) {
    free(array->data);
    array->size = 0;
    array->allocated = 0;
    array = NULL;
  }
}
int main (void) {

  return 0;
}
