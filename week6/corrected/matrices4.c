#include <stdio.h>
#include <stdlib.h>

#ifndef SIZE_MAX
#define SIZE_MAX (~(size_t)0)
#endif

typedef struct {
   double* m;
  /* Attention ici : on stocke le tableau en continu donc PAS DE double**. *
   * Ceux qui pr�f�rent double** auront une indirection de plus et un      *
   * tableau de pointeurs en plus en m�moire: perte de place !             *
   * Sans compter, comme vue en cours, que ces donn�es ne seront pas       *
   * continues en m�moire.                                                 */

   size_t lignes;
   size_t colonnes;
} Matrice;

Matrice* empty(Matrice*);
void libere(Matrice*);
Matrice* redimensionne(Matrice*, size_t lignes, size_t colonnes);
Matrice* lire_matrice(Matrice*);
void affiche_matrice(Matrice const *);
Matrice* multiplication(Matrice const * a, Matrice const * b,
                        Matrice* resultat);

/* ---------------------------------------------------------------------- */
int main(void)
{
  Matrice M1, M2, M3;

  (void) lire_matrice(&M1);
  /* On met cet appel � lire_matrice ici et non pas dans l'appel de    *
   * multiplication() car on ne peut garantir l'ordre d'�valuation des *
   * arguments de l'appel (� multiplication)) et donc on ne peut       *
   * garantir que la lecture de M1 sera faite avant celle de M2.       *
   * Mettre cet appel ici permet de le garantir.                       */

  if (multiplication(&M1, lire_matrice(&M2), empty(&M3))
      /* Attention � ne pas oublier d'initialiser M3 !! */
      == NULL) {
    printf("Multiplication de matrices impossible !\n");
  } else {
    printf("R�sultat :\n");
    affiche_matrice(&M3);
  }

  libere(&M1);
  libere(&M2);
  libere(&M3);
  return 0;
}

/* ---------------------------------------------------------------------- */
Matrice* empty(Matrice* resultat)
{
  if (resultat != NULL) {
    resultat->lignes   = 0    ;
    resultat->colonnes = 0    ;
    resultat->m        = NULL ;
  }
  return resultat;
}

/* ---------------------------------------------------------------------- */
void libere(Matrice* resultat)
{
  if (resultat != NULL) {
    if (resultat->m != NULL) free(resultat->m);
    (void) empty(resultat);
  }
}

/* ---------------------------------------------------------------------- */
Matrice* lire_matrice(Matrice* resultat)
{
  if (resultat != NULL) {
    size_t lignes;
    size_t colonnes;

    do {
      printf("Saisie d'une matrice :\n");
    
      do {
        printf("  Nombre de lignes : ");
        scanf("%u", &lignes);
      } while (lignes < 1);
    
      do {
        printf("  Nombre de colonnes : ");
        scanf("%u", &colonnes);
      } while (colonnes < 1);

      resultat->lignes   = lignes;
      resultat->colonnes = colonnes;

      if (SIZE_MAX / lignes < colonnes) {
        resultat->m = NULL;
      } else {
        resultat->m = calloc(lignes*colonnes, sizeof(*(resultat->m)));
      }
      if (NULL == resultat->m) {
        printf("Matrice trop grande pour �tre allou�e :-(\n");
      }

    } while (NULL == resultat->m);

    { size_t i, j;
      for (i = 0; i < lignes; ++i) 
        for (j = 0; j < colonnes; ++j) {
          printf("  M[%d,%d]=", i+1, j+1);
          scanf("%lf", &resultat->m[i*resultat->colonnes+j]);
        }
    }
  }
  return resultat;
}

/* ---------------------------------------------------------------------- */
Matrice* redimensionne(Matrice* resultat, size_t lignes, size_t colonnes)
{
  if (resultat != NULL) {
    if (SIZE_MAX / lignes < colonnes) return NULL;
    if (resultat->lignes*resultat->colonnes < lignes*colonnes) {
      if ((lignes*colonnes) > SIZE_MAX / sizeof(*(resultat->m))) return NULL;
      resultat->m = realloc(resultat->m, lignes*colonnes*sizeof(*(resultat->m)));
      if (NULL == resultat->m) {
        resultat->lignes   = 0;
        resultat->colonnes = 0;
        return NULL;
      } else {
        resultat->lignes   = lignes;
        resultat->colonnes = colonnes;
      }
    }
  }
  return resultat;
}

/* ---------------------------------------------------------------------- */
Matrice* multiplication(Matrice const * a, Matrice const * b,
                        Matrice* resultat)
{
  if (resultat != NULL) {
    size_t i, j, k;

    if ((a->colonnes == b->lignes) 
        && (redimensionne(resultat, a->lignes, b->colonnes) != NULL)) {
      for (i = 0; i < a->lignes; ++i) 
        for (j = 0; j < b->colonnes; ++j) {
          resultat->m[i*resultat->colonnes+j] = 0.0;
          for (k = 0; k < b->lignes; ++k) 
            resultat->m[i*resultat->colonnes+j] += a->m[i*a->colonnes+k] 
                                                   * b->m[k*b->colonnes+j];
        }
    } else {
      resultat = NULL;
    }
  }
  return resultat;
}

/* ---------------------------------------------------------------------- */
void affiche_matrice(Matrice const * matrice)
{
  size_t i, j;
  const size_t imax = matrice->lignes*matrice->colonnes;
  for (i = 0; i < imax; i += matrice->colonnes) {
    for (j = 0; j < matrice->colonnes; ++j) {
      printf("%g ", matrice->m[i+j]);
    }
    putchar('\n');
  }
}
