// C99

#include <stdio.h>  // pour les entr�es/sorties
#include <stdlib.h> // pour les allocations m�moire 

#ifndef SIZE_MAX
#define SIZE_MAX (~(size_t)0)
#endif

/* ---------------------------------------------------------------------- *
 * Types de donn�es                                                       *
 * ---------------------------------------------------------------------- */

typedef unsigned char IP_Addr[4]; // ou uint32_t de <stdint.h>

typedef struct _node {
  IP_Addr adresse; 
  const struct _node** voisins; // Attention aux DEUX �toiles ici !
  // const optionnel (mais on ne modifie pas ses voisins ;-) )
  size_t nb_voisins;
  /*
    Note : on pourrait aussi ajouter un nb_allocated_voisins et faire 
    de l'allocation de voisins page par page (au lieu de 1 par 1).
  */
} Noeud;

/* ---------------------------------------------------------------------- *
 * Prototypes (optionnel)                                                 *
 * ---------------------------------------------------------------------- */

Noeud* creation(const unsigned char adr1,
                const unsigned char adr2,
                const unsigned char adr3,
                const unsigned char adr4);

void sont_voisins(Noeud* p1, Noeud* p2);
// Pointeurs car les deux vont �tre modifi�s (ajout de voisins).
// Autre type de retour possible (e.g. code d'erreur).

int ajoute_voisin(Noeud* p1, const Noeud* p2);
// Pensez MODULAIRE !
// const pointeur pour le second, non modifi� ici.
// Retour : code d'erreur (optionnel, non utilis� d'ailleurs !)

unsigned int voisins_communs(const Noeud* p1, const Noeud* p2);
// const pointeurs pour �viter des copies inutiles.
// int ou size_t sont aussi valables commes type de retour.

void affiche(const Noeud* p);

void affiche_simple(const Noeud* p);
// Pensez MODULAIRE !

void libere(Noeud* p);
// NE PAS l'oublier !!

/* ---------------------------------------------------------------------- */
int main()
{
  Noeud* rezo[] = {
    creation(192, 168,  1, 1),
    creation(192, 168,  1, 2),
    creation(192, 168,  1, 3),
    creation(192, 168, 10, 1),
    creation(192, 168, 10, 2),
    creation(192, 168, 20, 1),
    creation(192, 168, 20, 2)
  };

  for (size_t i = 0 ; i < sizeof(rezo) / sizeof(rezo[0]); ++i) {
    if (NULL == rezo[i]) {
      fprintf(stderr, "pas assez de m�moire\n");
      exit(-1);
    }
  }    
    
  sont_voisins(rezo[0], rezo[1]);
  sont_voisins(rezo[0], rezo[2]);

  sont_voisins(rezo[1], rezo[2]);
  sont_voisins(rezo[1], rezo[3]);
  sont_voisins(rezo[1], rezo[5]);

  sont_voisins(rezo[2], rezo[3]);
  sont_voisins(rezo[2], rezo[5]);

  sont_voisins(rezo[3], rezo[4]);
  sont_voisins(rezo[3], rezo[5]);

  sont_voisins(rezo[5], rezo[6]);
  
  affiche(rezo[3]);

  affiche_simple(rezo[0]);
  printf(" et ");
  affiche_simple(rezo[5]);
  printf(" ont %u voisins communs.\n", voisins_communs(rezo[0], rezo[5]));

  affiche_simple(rezo[1]);
  printf(" et ");
  affiche_simple(rezo[2]);
  printf(" ont %u voisins communs.\n", voisins_communs(rezo[1], rezo[2]));
    
  /* garbage collecting */
  for (size_t i = 0 ; i < sizeof(rezo) / sizeof(rezo[0]); ++i) {
    libere(rezo[i]);
  }
  return 0;
}

/* ---------------------------------------------------------------------- *
 * D�finitions                                                            *
 * ---------------------------------------------------------------------- */

// ======================================================================
Noeud* creation(const unsigned char adr1,
                const unsigned char adr2,
                const unsigned char adr3,
                const unsigned char adr4)
{
  Noeud* bebe = malloc(sizeof(Noeud));
  if (NULL == bebe) {
    fprintf(stderr, "Erreur (creation) : impossible d'allouer de la m�moire "
            "pour un nouveau Noeud (%u.%u.%u.%u).\n", adr1, adr2, adr3, adr4);
    return NULL;
  }

  bebe->adresse[0] = adr1;
  bebe->adresse[1] = adr2;
  bebe->adresse[2] = adr3;
  bebe->adresse[3] = adr4;

  bebe->voisins = NULL;
  bebe->nb_voisins = 0;

  return bebe;
}

// ======================================================================
int ajoute_voisin(Noeud* p1, const Noeud* p2)
{
  if (p1 != NULL) {
    if (NULL == p2) { 
      fprintf(stderr, "Erreur (ajoute_voisin) : impossible d'ajouter un NULL-voisin\n");
      return 1;
    }

    ++(p1->nb_voisins);
    Noeud const ** const old_content = p1->voisins;
    if ((p1->nb_voisins > SIZE_MAX / sizeof(Noeud*)) ||
        /* NE PAS oublier de tester l'overflow ! */

        ((p1->voisins = realloc(p1->voisins, p1->nb_voisins * sizeof(Noeud*))) == NULL)
       ) {
      // echec
      p1->voisins = old_content;
      --(p1->nb_voisins);
      fprintf(stderr, "Erreur (ajoute_voisin) : %u.%u.%u.%u a d�j� trop de voisins.\n",
              p1->adresse[0] , p1->adresse[1], p1->adresse[2], p1->adresse[3]);
      return 2;
    }

    p1->voisins[p1->nb_voisins-1] = p2; 
    return 0;
  }
  return 3;
}

// ======================================================================
void sont_voisins(Noeud* p1, Noeud* p2)
{
  if (0 == ajoute_voisin(p1, p2)) {
    (void)ajoute_voisin(p2, p1);
  }
}

// ======================================================================
unsigned int voisins_communs(const Noeud* p1, const Noeud* p2)
{
  unsigned int voisins_commun = 0;

  if ((p1 != NULL) && (p2 != NULL)) {
    for (size_t i = 0; i < p1->nb_voisins; ++i) {
      for (size_t j = 0; j < p2->nb_voisins; ++j) {
        if (p1->voisins[i] == p2->voisins[j]) {
          ++voisins_commun;
        }
      }
    }
  }

  return voisins_commun;
}

// ======================================================================
void affiche(const Noeud* p)
{
  affiche_simple(p);
  printf(" a %zu voisins", p->nb_voisins);
  if (p->nb_voisins >= 1) {
    printf(" : ");
    if (p->nb_voisins >= 2) {
      for (size_t i = 0; i < p->nb_voisins - 1; ++i) {
        affiche_simple(p->voisins[i]);
        printf(", ");
      }
    }
    affiche_simple(p->voisins[p->nb_voisins - 1]);
    printf(".");
  }
  putchar('\n');
}

// ======================================================================
void affiche_simple(const Noeud* p)
{
  if (p != NULL) {
    printf("%u.%u.%u.%u"
           , p->adresse[0]
           , p->adresse[1]
           , p->adresse[2]
           , p->adresse[3]
           );
  } else {
    puts("(affiche_simple :) NULL");
  }
}

// ======================================================================
void libere(Noeud* p)
{
  free(p->voisins); 
  free(p); 
}

