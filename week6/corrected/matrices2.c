#include <stdio.h>
#include <stdlib.h>

#define N 10

typedef struct {
   double m[N][N];
   size_t lignes;
   size_t colonnes;
} Matrice;

Matrice* lire_matrice(void);
void affiche_matrice(Matrice const *);
Matrice* multiplication(Matrice const * a, Matrice const * b);

/* ---------------------------------------------------------------------- */
int main(void)
{
  Matrice* M1 = NULL;
  Matrice* M2 = NULL;
  Matrice* M  = NULL;

  M1 = lire_matrice();
  if (M1 != NULL) {
    M2 = lire_matrice();
    if (M2 != NULL) {
      if (M1->colonnes != M2->lignes) {
        printf("Multiplication de matrices impossible !\n");
      } else {
        printf("R�sultat :\n");
        M = multiplication(M1, M2);
        if (M != NULL) {
          affiche_matrice(M);
          free(M); M = NULL;
        }
      }
      free(M2); M2 = NULL;
    }
    free(M1); M1 = NULL;
  }
  return 0;
}

/* ---------------------------------------------------------------------- */
Matrice* lire_matrice(void)
{
  Matrice* resultat = NULL;
 
  /* On alloue la place m�moire pour la matrice de r�sultat */
  resultat = malloc(sizeof(Matrice));

  if (resultat != NULL) {
    size_t lignes;
    size_t colonnes;

    printf("Saisie d'une matrice :\n");
    
    do {
      printf("  Nombre de lignes (< %d) : ", N+1);
      scanf("%u", &lignes);
    } while ((lignes < 1) || (lignes > N));

    do {
      printf("  Nombre de colonnes (< %d) : ", N+1);
      scanf("%u", &colonnes);
    } while ((colonnes < 1)  || (colonnes > N));
    
    resultat->lignes = lignes;
    resultat->colonnes = colonnes;
    { size_t i, j;
    for (i = 0; i < lignes; ++i) 
      for (j = 0; j < colonnes; ++j) {
        printf("  M[%d,%d]=", i+1, j+1);
        scanf("%lf", &resultat->m[i][j]);
      }
    }
  }

  return resultat;
}

/* ---------------------------------------------------------------------- */
Matrice* multiplication(Matrice const * a, Matrice const * b)
{
  Matrice* resultat = NULL;

  /* On alloue la place m�moire pour la matrice de r�sultat */
  resultat = malloc(sizeof(Matrice));

  if (resultat != NULL) {
    size_t i, j, k;

    resultat->lignes   = a->lignes;
    resultat->colonnes = b->colonnes;

    if (a->colonnes == b->lignes) {
      for (i = 0; i < a->lignes; ++i) 
        for (j = 0; j < b->colonnes; ++j) {
          resultat->m[i][j] = 0.0;
          for (k = 0; k < b->lignes; ++k) 
            resultat->m[i][j] += a->m[i][k] * b->m[k][j];
        }
    }
    else {
     resultat = NULL;
    }
  }
  return resultat;
}

/* ---------------------------------------------------------------------- */
void affiche_matrice(Matrice const * matrice)
{
  size_t i, j;
  for (i = 0; i < matrice->lignes; ++i) {
    for (j = 0; j < matrice->colonnes; ++j) {
      printf("%g ", matrice->m[i][j]);
    }
    putchar('\n');
  }
}
