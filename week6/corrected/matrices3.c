#include <stdio.h>
#include <stdlib.h>

#define N 10

typedef struct {
   double m[N][N];
   size_t lignes;
   size_t colonnes;
} Matrice;

Matrice* lire_matrice(Matrice*);
void affiche_matrice(Matrice const *);
Matrice* multiplication(Matrice const * a, Matrice const * b,
                       /* pas de const ici, la valeur point�e par resultat *
                        * sera modifi�e.                                   */
                        Matrice* resultat); 

/* ---------------------------------------------------------------------- */
int main(void)
{
  Matrice M1, M2, M3;

  lire_matrice(&M1);
  if (multiplication(&M1, lire_matrice(&M2), &M3) == NULL) {
    printf("Multiplication de matrices impossible !\n");
  } else {
    printf("R�sultat :\n");
    affiche_matrice(&M3);
  }
  return 0;
}

/* ---------------------------------------------------------------------- */
Matrice* lire_matrice(Matrice* resultat)
{
  if (resultat != NULL) {
    size_t lignes;
    size_t colonnes;

    printf("Saisie d'une matrice :\n");
    
    do {
      printf("  Nombre de lignes (< %d) : ", N+1);
      scanf("%u", &lignes);
    } while ((lignes < 1) || (lignes > N));

    do {
      printf("  Nombre de colonnes (< %d) : ", N+1);
      scanf("%u", &colonnes);
    } while ((colonnes < 1)  || (colonnes > N));
    
    resultat->lignes = lignes;
    resultat->colonnes = colonnes;
    { size_t i, j;
    for (i = 0; i < lignes; ++i) 
      for (j = 0; j < colonnes; ++j) {
        printf("  M[%d,%d]=", i+1, j+1);
        scanf("%lf", &resultat->m[i][j]);
      }
    }
  }

  return resultat;
}

/* ---------------------------------------------------------------------- */
Matrice* multiplication(Matrice const * a, Matrice const * b,
                        Matrice* resultat)
{
  if (resultat != NULL) {
    size_t i, j, k;

    resultat->lignes   = a->lignes;
    resultat->colonnes = b->colonnes;

    if (a->colonnes == b->lignes) {
      for (i = 0; i < a->lignes; ++i) 
        for (j = 0; j < b->colonnes; ++j) {
          resultat->m[i][j] = 0.0;
          for (k = 0; k < b->lignes; ++k) 
            resultat->m[i][j] += a->m[i][k] * b->m[k][j];
        }
    } else {
      resultat = NULL;
    }
  }
  return resultat;
}

/* ---------------------------------------------------------------------- */
void affiche_matrice(Matrice const * matrice)
{
  size_t i, j;
  for (i = 0; i < matrice->lignes; ++i) {
    for (j = 0; j < matrice->colonnes; ++j) {
      printf("%g ", matrice->m[i][j]);
    }
    putchar('\n');
  }
}
