#include <stdio.h>
#include <stdlib.h>
#include <stdint.h> /* pour SIZE_MAX */

#define VECTOR_PADDING 512
#define TYPE int

#define vector_size(X) ((X)->size)

typedef struct {
  size_t size;
  size_t allocated;
  TYPE* content;
} vector;

/* ====================================================================== */
vector* construct_vector(vector* v) {
  vector* result;
  result = v;
  if (result != NULL) {
    result->content = calloc(VECTOR_PADDING, sizeof(TYPE));
    if (result->content != NULL) {
      result->size = 0;
      result->allocated = VECTOR_PADDING;
    } else {
      result = NULL;
    }
  }
  return result;
}

/* ====================================================================== */
void destruct_vector(vector* v) {
  if (v != NULL) {
    if (v->content != NULL) {
      free(v->content);
      v->content = NULL;
      v->size = v->allocated = 0;
    }
  }
}

/* ====================================================================== */
vector* create_vector(void) {
  vector* v = malloc(sizeof(vector));
  if (v != NULL) {
    if (construct_vector(v) == NULL) {
      free(v);
      v = NULL;
    }
  }
  return v;
}

/* ====================================================================== */
void delete_vector(vector* v) {
  if (v != NULL) {
    destruct_vector(v);
    free(v);
  }
}

/* ====================================================================== */
vector* empty_vector(vector* v) {
  if (v != NULL) {
      v->size = 0;
  }
  return v;
}

/* ====================================================================== */
vector* enlarge_vector(vector* v) {
  vector* result = v;
  if (result != NULL) {
    TYPE* const old_content = result->content;
    result->allocated += VECTOR_PADDING;
    if ((result->allocated > SIZE_MAX / sizeof(TYPE)) ||
        ((result->content = realloc(result->content,
                                    result->allocated * sizeof(TYPE)))
         == NULL)) {
      result->content = old_content;
      result->allocated -= VECTOR_PADDING;
      result = NULL;
    }
  }
  return result;
}

/* ====================================================================== */
size_t vector_push(vector* vect, TYPE val) {
  if (vect != NULL) {
    while (vect->size >= vect->allocated) {
      if (enlarge_vector(vect) == NULL) {
        return 0;
      }
    }
    vect->content[vect->size] = val;
    ++(vect->size);
    return vect->size;
  }
  return 0;
}

/* ====================================================================== */
int vector_set(vector* vect, size_t pos, TYPE val) {
  if (vect != NULL) {
    if (pos >= vect->size) vect->size = pos+1;
    while (vect->size >= vect->allocated) {
      if (enlarge_vector(vect) == NULL) {
        return 0;
      }
    }
    vect->content[pos] = val;
    return 1;
  }
  return 0;
}

/* ====================================================================== */
TYPE vector_get(vector const * vect, size_t pos) {
  TYPE result = (TYPE) 0;
  if (vect != NULL) {
    if (pos < vect->size) {
      result = vect->content[pos];
    }
  }
  return result;
}

/* ====================================================================== */
void print_vector(vector const * v) {
  register size_t i,j;
  printf("size: %d\n", vector_size(v));
  printf("allocated: %d\n", v->allocated);
  puts("elements:");
  for (i = 0, j = 0; i < vector_size(v); ++i, ++j) {
    printf("%d ", vector_get(v, i));
    if (j == 10) {
      putchar('\n');
      j = 0;
    }
  }
  putchar('\n');
}

/* ====================================================================== */
int main(void)
{
  vector v;
  construct_vector(&v);
  vector_push(&v, 2);
  vector_set(&v, 3, 2);
  print_vector(&v);
  destruct_vector(&v);
  return 0;
}
