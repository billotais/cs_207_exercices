#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h> /* pour isspace() */

/* nombre maximum de demandes en cas d'erreur */
#define NB_DEMANDES 3

/* nombre max. de r�ponses */
#define MAX_REP 10

typedef struct {
  char* question;
  char* reponses[MAX_REP];
  unsigned int nb_rep;
  unsigned int solution;
}  QCM;

typedef QCM* Examen;

void affiche(QCM const * question);
int demander_nombre(int min, int max);
unsigned int poser_question(QCM const * question);
unsigned int creer_examen(Examen*, FILE*);
int demander_fichier(FILE** f);
char* enlever_blancs(char* chaine);

/* ====================================================================== */
int main(void)
{
  unsigned int note = 0;
  Examen exam;
  unsigned int taille_examen;
  unsigned int i;
  FILE* fichier;

  if (! demander_fichier(&fichier)) {
    printf("=> j'abandonne !\n");
    return 1;
  } else {
    taille_examen = creer_examen(&exam, fichier);

    for (i = 0; i < taille_examen; ++i)
      if (poser_question(&(exam[i])) == exam[i].solution)
        ++note;

    printf("Vous avez trouv� %d bonne", note);
    if (note > 1) putchar('s');
    printf(" r�ponse");
    if (note > 1) putchar('s');
    printf(" sur %d.\n", taille_examen);
  }
  return 0;
}

/* ======================================================================
 * Fonction demander_fichier
 * ----------------------------------------------------------------------
 * In:   Un fichier (par r�f�rence) � ouvrir.
 * Out:  Ouvert ou non ?
 * What: Demande � l'utilisateur (au plus NB_DEMANDES fois) un nom de fichier
 *       et essaye de l'ouvrir en lecture.
 * ====================================================================== */
int demander_fichier(FILE** f)
{
  char nom_fichier[FILENAME_MAX+1];
  int taille_lue;
  unsigned short int nb = 0;

  do {
    ++nb;

    /* demande le nom du fichier */
    do {
      printf("Nom du fichier � lire : "); fflush(stdout);
      fgets(nom_fichier, FILENAME_MAX+1, stdin);
      taille_lue = strlen(nom_fichier) - 1;
      if ((taille_lue >= 0) && (nom_fichier[taille_lue] == '\n'))
        nom_fichier[taille_lue] = '\0';
    } while ((taille_lue < 1) && !feof(stdin));

    if (nom_fichier[0] == '\0') {
      *f = NULL;
      return 0;
    }

    /* essaye d'ouvrir le fichier */
    *f = fopen(nom_fichier, "r");

    /* est-ce que �a a march� ? */
    if (*f == NULL) {
      printf("-> ERREUR, je ne peux pas lire le fichier %s\n",
             nom_fichier);
    } else {
      printf("-> OK, fichier %s ouvert pour lecture.\n",
             nom_fichier);
    }
  } while ((*f == NULL) && (nb < NB_DEMANDES));
  
  return (*f != NULL);
}

/* ====================================================================== */
void affiche(QCM const * q)
{
  unsigned int i;
  printf("%s ?\n", q->question);
  for (i = 0; i < q->nb_rep; ++i) {
    printf("    %d- %s\n", i+1, q->reponses[i]);
  }
}

/* ====================================================================== */
int demander_nombre(int a, int b)
{
   int res;

   if (a > b) { res=b; b=a; a=res; }

   do {
       printf("Entrez un nombre entier compris entre %d et %d :\n",
              a, b);
       scanf("%d", &res);
    } while ((res < a) || (res > b));
    return res;
}

/* ====================================================================== */
unsigned int poser_question(QCM const * q)
{
  affiche(q);
  return (unsigned) demander_nombre(1, q->nb_rep);
}

/* ====================================================================== */
unsigned int creer_examen(Examen* retour, FILE* fichier)
{
  QCM* question;
  int erreur = 0;       /* une erreur de format s'est produite */
  int dansquestion = 0; /* en train de lire une question */
#define TAILLE_LIGNE 1025
  char line[TAILLE_LIGNE];      /* ligne � lire */

  unsigned int nb_quest = 0;

  /* taille maximale d'un examen 
     Dans une version plus avanc�e on pr�f�rera une allocation plus dynamique
     � base de realloc.
   */
#define MAX_QUESTIONS 1000
  *retour = calloc(MAX_QUESTIONS, sizeof(QCM));
  if (*retour == NULL) {
    fprintf(stderr, "Erreur: je ne peux pas allouer %d questions.\n"
            "Recompilez avec un MAX_QUESTIONS plus petit.\n",
            MAX_QUESTIONS);
    return 0;
  }

  /* On fait pointer question sur le premier QCM de l'examen (retour) */
  question = *retour;

  do {
    char *rep;
    rep = fgets(line, TAILLE_LIGNE, fichier); /* lit une ligne */

    if ((rep != NULL) && (line[0] != '#')) { 
      /* si c'est bien une ligne et ce n'est pas un commentaire */

      if ((line[0] != 'Q') || (line[1] != ':')) {
        /* si la ligne ne commence pas par "Q:" */

        if (! dansquestion) {
          /* Si on n'a pas encore eu de question : qqchose ne va pas ! */
          fprintf(stderr, "Mauvais format de fichier : pas de \"Q:\"\n");
          erreur = 1;
        } else {
          /* on a d�j� eu une question => c'est donc une ligne de r�ponse :
             lecture d'une r�ponse � la question */

          ++(question->nb_rep);
          if (question->nb_rep > MAX_REP) {
            fprintf(stderr, "Je ne peux pas accepter plus que %d r�ponses\n",
                    MAX_REP);
            erreur = 1;
          }

          else {
            char** current = &(question->reponses[question->nb_rep - 1]);
            if ((line[0] == '-') && (line[1] == '>')) {
              /* la r�ponse correcte est recopi�e */
              *current = malloc(strlen(line)-1); /* remarquez qu'ici strlen(line) est forcement >= 2 */
              if (*current == NULL) {
                fprintf(stderr,
                        "Erreur: plus de place pour allouer une r�ponse\n");
                return 0;
              }
              strcpy(*current, &(line[2])); /* supprime le "->" initial */
              if (question->solution != 0) {
                fprintf(stderr, 
                        "Hmmm bizard, j'avais d�j� une r�ponse correcte pour"
                        " cette question !\n");
              } else if (enlever_blancs(*current)[0] == '\0') {
                fprintf(stderr, 
                        "Hmmm bizard, la r�ponse indiqu�e est vide ! "
                        "Cela n'a pas de sens !\n");
                erreur = 1;
              } else {
                question->solution = question->nb_rep;
              }
            } else { /* autre reponse possible */
              if (enlever_blancs(line)[0] == '\0') {
                /* ligne vide -> ignorer */
                --(question->nb_rep);
              } else {
                *current = malloc(strlen(line)+1);
                if (*current == NULL) {
                  fprintf(stderr,
                          "Erreur: plus de place pour allouer une r�ponse\n");
                  return 0;
                }
                strcpy(*current, line);
              }
            }
          }
        }
      } else { /* ligne de question : "Q: ..." */
        if (dansquestion) /* passe a la question suivante */
          ++question; 

        /* la question est recopi�e */
        question->question = malloc(strlen(line)-1);  /* remarquez qu'ici strlen(line) est forcement >= 2 (line commence par "Q:") */
        strcpy(question->question, &(line[2])); /* recopie sans le "Q:" initial */
        if (enlever_blancs(question->question)[0] == '\0') {
          fprintf(stderr, 
                  "Hmmm bizard, la question est vide ! Cela n'a pas de sens !\n");
          erreur = 1;
        } else {
          ++nb_quest;
          question->nb_rep = 0;    /* remets � z�ro les r�ponses... */
          question->solution = 0; /* ...et la solution correcte */
          dansquestion = 1;   /* on a une question */
        }
      }
    }
  } while (!feof(fichier) && !erreur);

  return nb_quest;
}

/* ====================================================================== */
char* enlever_blancs(char* chaine)
{ /* Supprime les blancs initiaux et finaux d'une chaine */

  unsigned char* c1 = NULL;
  unsigned char* c2 = NULL;

  /* Attention ! unsigned est primordial ici pour les caract�res >= 128 
     (par exemple les accents). Sinon le bit de signe, lors du passage en int, 
     va se balader � la mauvaise place !! 
     Par exemple isspace('�') renvoie 8 !! */

  /* supprime les blancs finaux */
  for (c1 = (unsigned char *) (chaine + strlen(chaine) - 1); 
       (c1 >= (unsigned char *) chaine) && isspace(*c1); --c1)
    *c1 = '\0';

  /* supprime les blancs initiaux */
  if (isspace((unsigned char) chaine[0])) {
    for (c1 = (unsigned char *) chaine; *c1 && isspace(*c1); ++c1);
    for (c2 = c1; *c2; ++c2)
      chaine[c2-c1] = *c2;
    chaine[c2-c1] = '\0'; }
  
  return chaine;
}
