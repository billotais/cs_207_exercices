#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXARGS 10

#define MAXSTR  32
#define MAXLINE 256

/* un noeud de l'arbre */
typedef struct addr__ addr_t;
struct addr__ {
    addr_t *left;		/* noeud gauche */
    addr_t *right;		/* noeud droite */

    char name[MAXSTR];	/* cl� du noeud */
    char num [MAXSTR];	/* valeur du noeud */

    /*  valeurs suppl�mentaires ici */
};

/* l'arbre, d�fini par sa racine */
typedef struct book__ {
    addr_t* root;
} book_t;

/* --------------------------------------------------- */

/* cr�er un nouveau carnet d'adresses vide */
book_t* book_create(void);

/* lib�rer les ressources associ�es */
void book_free(book_t* b);

/* ajouter ou modifier une entr�e du carnet d'adresse */
void book_add(book_t* b, const char* name, const char* num);

/* supprimer une entr�e du carnet d'adresses */
void book_remove(book_t* b, const char* name);

/* lister dans l'ordre tous les noms du carnet d'adresses */
void book_list(book_t* b);

/* afficher une entr�e du carnet d'adresses */
void book_view(book_t* b, const char* name);

/* remplacer le contenu du carnet d'adresses par celui du fichier */
void book_load(book_t* b, const char* file);

/* sauver le contenu du carnet dans un fichier au format CSV */
void book_save(book_t* b, const char* file);

/*
 * Note: le format CSV est: un enregistrement par ligne, o� les
 * champs sont s�par�s par des ';'. Par exemple:
 *
 * nom1;num�ro1;\n
 * nom2;num�ro2;\n
 * ...
 *
 */

/* --------------------------------------------------- */

/*
 * donne l'adresse du pointeur qui conduit � l'enregistrement
 * - si l'enregistrement n'existe pas, retourne l'adresse du pointeur
 *   � modifier pour rajouter l'enregistrement
 * - si l'enregistrement existe, retourne l'adresse du pointeur
 *   vers l'enregistrement
 */
addr_t** book_find(book_t*, const char* name);

/* manipulateurs d'adresse (notamment pour la r�cursivit� */
addr_t* addr_create(const char* name, const char* num);
void addr_free(addr_t* a);
void addr_list(addr_t* a);
addr_t* addr_read(FILE* f);
void addr_write(addr_t* a, FILE* f);

/* ====================================================================== */


/* copie <src> dans <dst> qui a la taille <size>, tronque si besoin */
#if !defined(__APPLE__) && !defined(__OpenBSD__) && !defined(__FreeBSD__)
size_t strlcpy(char* dst, const char* src, size_t size)
{
    size_t len = strlen(src);
    size_t real = (len>size-1) ? size-1 : len;
    strncpy(dst, src, real);
    dst[real] = 0;
    return len;
}
#endif

/* --------------------------------------------------- */

addr_t* addr_create(const char* name, const char* num)
{
    addr_t* a = malloc(sizeof(addr_t));
    if (!a) return NULL;
    a->left=NULL;
    a->right=NULL;
    strlcpy(a->name, name, MAXSTR);
    strlcpy(a->num, num, MAXSTR);
    return a;
}

void addr_list(addr_t* a)
{
    if (a->left) addr_list(a->left);
    printf("  - %s\n",a->name);
    if (a->right) addr_list(a->right);
}

void addr_free(addr_t* a)
{
    if (a==NULL) return;
    if (a->left) addr_free(a->left);
    if (a->right) addr_free(a->right);
    free(a);
}

addr_t* addr_read(FILE* f)
{
    char buf[MAXLINE];
    if (fgets(buf,MAXLINE,f)) {
        char* num=NULL;
        char* p;
        if ((p=strchr(buf,'\n'))) *p=0;
        if ((p=strchr(buf,';'))) {
            *p=0;
            num = &p[1];
            if ((p=strchr(num,';'))) *p=0;
            return addr_create(buf,num);
        }
    }
    return NULL;
}

void addr_write(addr_t* a, FILE* f)
{
    if (a->left) addr_write(a->left, f);
    fprintf(f,"%s;%s;\n",a->name,a->num);
    if (a->right) addr_write(a->right, f);
}

/* --------------------------------------------------- */

book_t* book_create(void)
{
    book_t* b = malloc(sizeof(book_t));
    if (b==NULL) return NULL;
    b->root=NULL;
    return b;
}

void book_free(book_t* b)
{
    if (b==NULL) return;
    if (b->root!=NULL) addr_free(b->root);
    free(b);
}

addr_t** book_find(book_t* b, const char* name)
{
    int found = 0; /* si un enregistrement existe d�j� */
    addr_t **pp = &b->root; /* pointeur vers le pointeur qui r�f�rence l'adresse */

    /* tant qu'on ne trouve pas un pointeur NULL (une feuille)
     * ou la valeur qu'on cherche, on parcourt la structure */
    while((*pp!=NULL) && found==0) {
        addr_t *a = *pp; /* on r�cup�re un pointeur vers l'adresse r�f�renc�e */
        int cmp = strcmp(a->name, name);
        if (cmp>0) pp =  &(a->left);
        else if (cmp<0) pp = &(a->right);
        else found=1; /* pp pointe a */
    }
    return pp;
}

void book_add(book_t* b, const char* name, const char* num)
{
    addr_t **pp = book_find(b, name);
    if (*pp != NULL) {
        addr_t *a = *pp;
        strlcpy(a->num, num, MAXSTR); /* une adresse existe, on la met � jour */
    } else {
        addr_t *a = addr_create(name, num);
        if (a != NULL) *pp = a; /* on cr�e l'adresse qu'on attache au pointeur */
    }
}

void book_remove(book_t* b, const char* name)
{
    addr_t **pp = book_find(b, name);
    if (*pp == NULL) printf("Pas trouv�.\n");
    else {
        addr_t *a = *pp; /* on prend l'enregistrement */
        *pp = NULL; /* on la d�connecte de l'arbre */
        /* on remet les enfants dans l'arbre */
        if (a->left)  {
            pp = book_find(b, a->left->name );
            *pp = a->left ;
        }
        if (a->right) {
            pp = book_find(b, a->right->name);
            *pp = a->right;
        }
        free(a);
    }
}

void book_list(book_t* b)
{
    if (b->root) addr_list(b->root);
    else printf("le carnet d'adresses est vide.\n");
}

void book_view(book_t* b, const char* name)
{
    addr_t **pp = book_find(b, name);
    if (*pp==NULL) printf("Pas trouv�.\n");
    else {
        addr_t *a = *pp; /* on prend l'enregistrement */
        printf("Vous pouvez appeler %s au num�ro %s.\n", a->name, a->num);
    }
}

void book_load(book_t* b, const char* file)
{
    char fname[MAXLINE];
    snprintf(fname, MAXLINE, "%s.csv", file);
    FILE* f = fopen(fname, "r");
    if (f != NULL) {
        /* on vide le carnet d'adresses */
        if (b->root) {
            addr_free(b->root);
            b->root=NULL;
        }

        /* on lit les adresses */
        addr_t *a;
        while ((a=addr_read(f))) {
            addr_t **pp = book_find(b, a->name);
            *pp = a;
        }

        fclose(f);
    } else {
        printf("Impossible de lire %s.\n",fname);
    }
}

void book_save(book_t* b, const char* file)
{
    char fname[MAXLINE];
    snprintf(fname, MAXLINE, "%s.csv", file);
    FILE* f = fopen(fname, "w");
    if (f != NULL) {
        if (b->root) addr_write(b->root, f);
        fclose(f);
    } else {
        printf("Impossible d'�crire %s.\n",fname);
    }
}

int main(void)
{
    int quit = 0;
    book_t *b = book_create();

    /* donn�es de test */
    book_add(b,"Lucien"  ,"012 345 67 89");
    book_add(b,"St�phane","021 879 51 32");
    book_add(b,"Julien"  ,"079 523 12 45");
    book_add(b,"Antoine" ,"076 125 08 78");
    book_add(b,"Damien"  ,"022 329 08 85");

    while (!quit) {
        /* on lit la commande dans un buffer */
        char cmd[MAXSTR*2];
        printf("> ");
        fflush(stdout);
        char* check = fgets(cmd,MAXSTR*2,stdin);

        if ((check != NULL) && (cmd[0] != '\n')) {

            /* on d�coupe la commande en arguments, voir man strsep  */
            int  an = 0;		/* nombre d'arguments  */
            char* a[MAXARGS];	/* tableau d'arguments */
            char* ptr = cmd;
            while (an<MAXARGS && ((a[an]=strtok(ptr," \t\n")) != NULL)) {
                if (a[an][0]!='\0') ++an;
                ptr = NULL;
            }

            if (a[0] != NULL) {
                /* on interpr�te la commande */
                if (!strcmp(a[0],"add") && an>2) book_add(b,a[1],a[2]);
                else if (!strcmp(a[0],"del") && an>1) book_remove(b,a[1]);
                else if (!strcmp(a[0],"view")) book_view(b,a[1]);
                else if (!strcmp(a[0],"list")) book_list(b);
                else if (!strcmp(a[0],"load") && an>1) book_load(b,a[1]);
                else if (!strcmp(a[0],"save") && an>1) book_save(b,a[1]);
                else if (!strcmp(a[0],"quit")) {
                    printf("Au revoir.\n");
                    quit=1;
                } else if (!strcmp(a[0],"help")) {
                    printf("  add <name> <num>  ajouter un num�ro\n");
                    printf("  del <name>        supprimer un num�ro\n");
                    printf("  view <name>       afficher les informations\n");
                    printf("  list              lister les noms\n");
                    printf("  load <file>       lit les adresses du fichier\n");
                    printf("  save <file>       enregistre les adresses dans le fichier\n");
                    printf("  quit              quitter le programme\n");
                } else printf("Commande erronn�e, entrez 'help' pour l'aide.\n");
            }
        }
    }
    book_free(b);
    return 0;
}
