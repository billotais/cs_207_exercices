#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_REP 10

/* Types */
typedef struct {
  char* question;
  char* reponses[MAX_REP]; /* tableau de 10 pointeurs de caract�res */
  unsigned int nb_rep;
  unsigned int solution;
}  QCM;

typedef QCM* Examen;

/* Prototypes */
void affiche(QCM const * question);
int demander_nombre(int min, int max);
unsigned int poser_question(QCM const * question);
unsigned int creer_examen(Examen*);
void detruire_examen(Examen*);

/* ====================================================================== */
int main(void)
{
  unsigned int note = 0;
  Examen exam = NULL;
  unsigned int taille_examen = creer_examen(&exam);
  unsigned int i;

  for (i = 0; i < taille_examen; ++i)
    if (poser_question(&(exam[i])) == exam[i].solution)
      ++note;

  /* petite astuce pour accorder 'bonne reponse' si 
   * l'utilisateur a plusieurs r�ponses correctes. 
   */
  printf("Vous avez trouv� %d bonne", note);
  if (note > 1) putchar('s');
  printf(" r�ponse");
  if (note > 1) putchar('s');
  printf(" sur %d.\n", taille_examen);

  detruire_examen(&exam);
  return 0;
}


/* ====================================================================== */
void affiche(QCM const * q)
{
  unsigned int i;
  printf("%s ?\n", q->question);
  for (i = 0; i < q->nb_rep; ++i) {
   /* on affiche i+1 pour �viter de commencer l'�num�ration des r�ponses avec 0 */
    printf("    %d- %s\n", i+1, q->reponses[i]);
  }
}

/* ====================================================================== */
int demander_nombre(int a, int b)
{
   int res;

   if (a > b) { res=b; b=a; a=res; }

   do {
       printf("Entrez un nombre entier compris entre %d et %d : ",
              a, b);
       scanf("%d", &res);
    } while ((res < a) || (res > b));
    return res;
}

/* ====================================================================== */
unsigned int poser_question(QCM const * q)
{
  affiche(q);
  /* on transforme le type int retourn� par demander_nombre en un unsigned int */
  return (unsigned int) demander_nombre(1, q->nb_rep);
}

/* ====================================================================== */
unsigned int creer_examen(Examen* retour)
{
  unsigned int i;

  /* Pour cet examen, on a 3 QCM, donc il faut allouer l'�quivalent de
   * 3 fois la taille d'un QCM dans la m�moire.
   */
  *retour = calloc(3, sizeof(QCM));

  
  /* QUESTION 1 */
  /* On alloue une taille de 50 caract�res pour la question.
   * Note: malloc(50) ou malloc(50*sizeof(char)) revient
   * au m�me car sizeof(char) est toujours �gal � 1.
   */
  (*retour)[0].question = malloc(50);
  strcpy((*retour)[0].question,
         "Combien de dents poss�de un �l�phant adulte");

  (*retour)[0].nb_rep = 5;
  
  for (i = 0; i < (*retour)[0].nb_rep; ++i) {
  /* On alloue 10 caract�res pour chaque r�ponse. */
    (*retour)[0].reponses[i] = malloc(10);
  }
  strcpy((*retour)[0].reponses[0], "32");
  strcpy((*retour)[0].reponses[1], "de 6 � 10");
  strcpy((*retour)[0].reponses[2], "beaucoup");
  strcpy((*retour)[0].reponses[3], "24");
  strcpy((*retour)[0].reponses[4], "2");

  (*retour)[0].solution = 2;

  /* QUESTION 2 */
  /* On alloue 80 caract�res pour la question. */
  (*retour)[1].question = malloc(80);
  strcpy((*retour)[1].question,
         "Laquelle des instructions suivantes est un prototype de fonction");

  (*retour)[1].nb_rep = 4;

  for (i = 0; i < (*retour)[1].nb_rep; ++i) {
    /* On alloue 14 caract�res pour chaque r�ponse. */
    (*retour)[1].reponses[i] = malloc(14);
  }
  strcpy((*retour)[1].reponses[0], "int f(0);");
  strcpy((*retour)[1].reponses[1], "int f(int 0);");
  strcpy((*retour)[1].reponses[2], "int f(int i);");
  strcpy((*retour)[1].reponses[3], "int f(i);");

  (*retour)[1].solution = 3;

  /* QUESTION 2 */
  /* On alloue 40 caract�res pour la question. */
  (*retour)[2].question = malloc(40);
  strcpy((*retour)[2].question,
         "Qui pose des questions stupides");

  (*retour)[2].nb_rep = 7;

  for (i = 0; i < (*retour)[2].nb_rep; ++i) {
    /* On alloue 50 caract�res pour chaque r�ponse. */
    (*retour)[2].reponses[i] = malloc(50);
  }
  strcpy((*retour)[2].reponses[0], "le prof. de math");
  strcpy((*retour)[2].reponses[1], "mon copain/ma copine");
  strcpy((*retour)[2].reponses[2], "le prof. de physique");
  strcpy((*retour)[2].reponses[3], "moi");
  strcpy((*retour)[2].reponses[4], "le prof. d'info");
  strcpy((*retour)[2].reponses[5], "personne, il n'y a pas de question stupide");
  strcpy((*retour)[2].reponses[6], "les sondages");

  (*retour)[2].solution = 6;
         
  return (unsigned int) 3;
}

/* ====================================================================== */
void detruire_examen(Examen* retour)
{
  unsigned int i, j;

  /* Pour chaque question */
  for (i = 0; i < 3; ++i) {
  
    /* Pour chaque reponse � cette question */
    for (j = 0; j < (*retour)[i].nb_rep; ++j) 
    {   
      /* On lib�re la m�moire allou�e pour chaque reponse */
      free((*retour)[i].reponses[j]);
    }
    /* On lib�re la m�moire allou�e pour chaque question */
    free((*retour)[i].question);
  }
  /* On lib�re la m�moire allou�e pour l'Examen point� par retour */
  free(*retour);
  *retour = NULL;
}


