#include <stdio.h>
#include <stdlib.h>

/* ======================================================================
 * ARBRES - Data structures et Prototypes
 * ====================================================================== */

typedef struct arbre_ Arbre;
struct arbre_ {
  char valeur;
  Arbre* gauche;
  Arbre* droite;

  /* necessaire si pointages multiples possibles, ie si le m�me sous-arbre
     est utilis� par diff�rents noeuds p�res, par exemple dans des arbres
     diff�rents.
     Une alternative serait d'utiliser des deep copies dans creer_arbre au
     lieu de faire pointer tout le monde au m�me endroit, mais cette
     derni�re solution est plus lourde.
  */
  unsigned int nb_acces;
};

Arbre* creer_arbre(char valeur, Arbre* g, Arbre* d);
void affiche_arbre(Arbre* a);
void libere_arbre(Arbre** a);

/* ======================================================================
 * ARBRES - Definitions
 * ====================================================================== */

Arbre* creer_arbre(char valeur, Arbre* g, Arbre* d) 
{
  Arbre* arbre;

  arbre = malloc(sizeof(Arbre));
  if (arbre == NULL) {
    fprintf(stderr, "Erreur : plus assez de m�moire pour faire pousser "
            "un nouvelle arbre\n");
  } else {
    arbre->valeur = valeur;
    arbre->gauche = g; 
    if (g != NULL) ++(g->nb_acces);
    arbre->droite = d; 
    if (d != NULL) ++(d->nb_acces);
    arbre->nb_acces = 0;
  }

  return arbre;
}

void affiche_arbre(Arbre* a)
{
  if (a != NULL) {
    if (a->gauche != NULL) {
      putchar('(');
      affiche_arbre(a->gauche);
      putchar(' ');
    }
    printf("%c", a->valeur);
    if (a->droite != NULL) {
      putchar(' ');
      affiche_arbre(a->droite);
      putchar(')');
    }
  }
}

void libere_arbre(Arbre** pa)
{
  if (pa != NULL) {
    Arbre* const a = *pa; // juste pour simplifier l'�criture
    if (a != NULL) {
      if (a->nb_acces > 0) { --(a->nb_acces); }
      if (a->nb_acces == 0) {
        libere_arbre(&(a->gauche));
        libere_arbre(&(a->droite));
        free(a);
        *pa = NULL; /* pas "a" ici, bien s�r ! */
      }
    }
  }
}

/* ======================================================================
 * DERIVATION
 * ====================================================================== */

Arbre* derive(Arbre* arbre, char variable)
{
  Arbre* resultat;
  Arbre* derive_gauche;
  Arbre* derive_droite;

  if (arbre == (Arbre*) NULL)
    return arbre;

  /* Pre-calcul des derives a gauche et a droite */
  derive_gauche = derive(arbre->gauche, variable);
  derive_droite = derive(arbre->droite, variable);

  switch (arbre->valeur) {
  case '+': 
  case '-':
    resultat = creer_arbre(arbre->valeur, derive_gauche, derive_droite);
    break;

  case '*':
      resultat =
        creer_arbre('+', 
                    creer_arbre('*', derive_gauche, arbre->droite),
                    creer_arbre('*', arbre->gauche, derive_droite)
                    );
    break;

  case '/':
      resultat =
        creer_arbre('/',
                    creer_arbre('-',
                                creer_arbre('*', derive_gauche, arbre->droite),
                                creer_arbre('*', arbre->gauche, derive_droite)),
                    creer_arbre('*', arbre->droite, arbre->droite)
                    );
      break;

  case '^':
    resultat =
      creer_arbre('*',
                  creer_arbre('*', arbre->droite, derive_gauche),
                  creer_arbre('^', arbre->gauche, 
                              creer_arbre('-',
                                          arbre->droite,
                                          creer_arbre('1',NULL,NULL)))
                  );
    libere_arbre(&derive_droite); // on ne fait pas de f(x)^g(x) ;-)
    break;

  default:
    if (arbre->valeur == variable)
      resultat = creer_arbre('1', NULL, NULL);
    else 
      resultat = creer_arbre('0', NULL, NULL);
  }

  return resultat;
}

/* ======================================================================
 * MAIN
 * ====================================================================== */

int main(void)
{
  Arbre *a = creer_arbre('a', NULL, NULL);
  Arbre *b = creer_arbre('b', NULL, NULL);
  Arbre *x = creer_arbre('x', NULL, NULL);
  Arbre *d_expr1, *d_expr2, *d_expr3,*d_expr4;

  Arbre *xpa = creer_arbre('+', x, a);
  Arbre *expr1 = xpa;
  Arbre *xpb = creer_arbre('+', x, b);
  Arbre *x2  = creer_arbre('*', x, x);

  Arbre *expr2 = creer_arbre('*', xpa, xpb);
  Arbre *expr3 = creer_arbre('+', creer_arbre('*', x, x2),
                             creer_arbre('*', a, x));
  Arbre *expr4 = creer_arbre('/', creer_arbre('^', x, a), 
                             creer_arbre('+', x2, creer_arbre('*', b, x)));

  affiche_arbre(expr1); printf("\n");
  affiche_arbre(expr2); printf("\n");
  affiche_arbre(expr3); printf("\n");
  affiche_arbre(expr4); printf("\n");

  /* Calcul des derivees */
  d_expr1 = derive(expr1,'x');
  d_expr2 = derive(expr2,'x');
  d_expr3 = derive(expr3,'x');
  d_expr4 = derive(expr4,'x');

  /* Afficher les derivees */
  printf("d(expr1)/dx = ");
  affiche_arbre(d_expr1);
  printf("\nd(expr2)/dx = ");
  affiche_arbre(d_expr2);
  printf("\nd(expr3)/dx = ");
  affiche_arbre(d_expr3);
  printf("\nd(expr4)/dx = ");
  affiche_arbre(d_expr4);
  printf("\n");

  /* liberation de la memoire */ 
  libere_arbre(&d_expr1);
  libere_arbre(&d_expr2);
  libere_arbre(&d_expr3);
  libere_arbre(&d_expr4);
  libere_arbre(&expr2);
  libere_arbre(&expr3);
  libere_arbre(&expr4);

  return 0;
}
