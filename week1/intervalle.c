#include <stdio.h>

int main(void)
{
    printf("Entrez un nombre : ");
    double x;
    scanf("%lf", &x);

    if (x >= -1 && x < 1) {
        printf("x appartient à I1\n");
    }
    else {
        printf("x n'appartient pas à I1\n");
    }


    if (x < -10 || x == 3 || 3 < x || (-2 < x && x < 0) || x == 0 || (x < 2 && 1 < x)) {
        printf("x n'appartient pas à I2\n");
    }
    else {
        printf("x appartient à I2\n");
    }
    return 0;
}