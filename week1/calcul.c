#include <stdio.h>

int main(void)
{
    double x;
    double y;
    double a, b, c, d;
    
    x = 2;
    y = 4;
    a = x + y;
    b = x - y;
    c = x * y;
    d = x / y;
    printf("a = %lf, b = %lf, c = %lf, d = %lf\n", a, b, c, d);

    return 0;
}