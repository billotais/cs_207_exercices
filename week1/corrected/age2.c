/* C89 */
#include <stdio.h>

int main(void)
{
  /* d�clarations et initialisations */
  int age = 18;                              
  int annee = 2017;

  printf("Entrez votre �ge :\n"); /* Affichage de la question */
  scanf("%d", &age );             /* Lecture de la r�ponse    */
  annee -=  age;                  /* Calcul                   */

  /* Affichage de l'ann�e */
  printf("Vous �tes n�(e) vers l'ann�e %d\n", annee);
  return 0;
}
