/* C89 */
#include <stdio.h>

int main(void)
{
  /* D�clarations */
  int age;
  int annee;

  printf("Entrez votre �ge :\n"); /* Affichage de la question */
  scanf("%d", &age);              /* Lecture de la r�ponse    */
  annee = 2017 - age;             /* Calcul                   */

  /* Affichage de l'ann�e */
  printf("Vous �tes n�(e) vers l'ann�e %d\n", annee);
  return 0;
}
