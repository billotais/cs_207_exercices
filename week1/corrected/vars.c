/* C89 */
#include <stdio.h>

int main(void)
{
  int x, y;                           /* d�clarations */
  double a = 0.0, b = 0.0, c = 0.0, d = 0.0;
  x=2; y=4;                           /* affectations ; puisque la donn�e demande des affectations.
                                       * En toute rigueur, il aurait �t� pr�f�rable de faire des initialisations...
                                       */
  a=x+y; b=x-y; c=x*y; d=x/y;         /* op�rations   */
                                      /* affichage des r�sultats */
  printf(" 2+4=%f  2-4=%f  2*4=%f  2/4=%f\n", a, b, c, d);
  return 0;
}
