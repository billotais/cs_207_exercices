#include <stdio.h>

int main(void) {
  double x = 0.0;               /* d�claration et initialisation */
  printf("Entrez un r�el :\n"); /* demande � l'utilisateur d'entrer un r�el */
  scanf("%lf", &x);             /* enregistre la r�ponse dans x */

  if ( (x >= -1.0) && (x < 1.0) ) { 
      printf("x appartient � I\n");
  } else {
      printf("x n'appartient pas � I\n");
  }
  return 0;
}
