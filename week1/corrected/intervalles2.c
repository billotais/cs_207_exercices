#include <stdio.h>

int main(void) {
  double x = 0.0;               /* d�claration et initialisation */
  printf("Entrez un r�el :\n"); /* demande � l'utilisateur d'entrer un r�el */
  scanf("%lf", &x);             /* enregistre la r�ponse dans x */

  if (   (!(x < 2.0)                &&  (x <  3.0)                )
      || (!(x < 0.0) && !(0.0 == x) && ((x <  1.0) || ( 1.0 == x)))
      || (!(x < -10.0)              && ((x < -2.0) || (-2.0 == x)))
     ) {
      printf("x appartient � I\n");
  } else {
      printf("x n'appartient pas � I\n");
  }
  return 0;
}
