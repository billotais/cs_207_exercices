#include <stdio.h>
#define TAILLE_FICHIER 10
#define TAILLE_NOM 20

int main(void) {
  char nom_fichier[TAILLE_FICHIER] = "data.dat";
  FILE* sortie = fopen(nom_fichier, "w");
  if (sortie == NULL) {
    fprintf(stderr, "Erreur : impossible d'ouvir le fichier %s\n", nom_fichier);
  }
  else {
    do {
      char nom[TAILLE_NOM] = "";
      int age = 0;
      printf("Entrez un nom (CTRL-D pour terminer le programme) : ");
      scanf("%s", nom);
      if (!feof(stdin) && !ferror(stdin)){
        printf("Age : ");
        scanf("%d", &age);
        fprintf(sortie, "%-10s %2d\n", nom, age);
      }
    } while (!feof(stdin) && !ferror(stdin));
  }
  fclose(sortie);
  return 0;
}
