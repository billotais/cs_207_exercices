#include <stdio.h>
#include <string.h>
#define FILENAME_MAX 10

FILE* demander_fichier(void) {
  char nom_fichier[FILENAME_MAX+1] = "";
  FILE* entree = NULL;

  int len;
  int count = 0;
  do {
    printf("Nom du fichier :");
    fscanf(stdin, "%s", nom_fichier);
    entree = fopen(nom_fichier, "r");
    count += 1;
  } while(count < 4 && entree != 1);
}
int main(void) {
  demander_fichier();
  return 0;
}
