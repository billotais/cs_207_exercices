#include <stdio.h>
#include <limits.h>
#define TAILLE_NOM 15
#define TAILLE_FICHIER 10

int main (void) {
  char const nom_fichier[TAILLE_FICHIER] = "data.dat";
  FILE* entree = NULL;

  entree = fopen(nom_fichier, "r");
  if (entree == NULL) {
    fprintf(stderr, "Erreur");
  }
  else {
    char nom[TAILLE_NOM + 1];
    int age = 0;
    int count = 0;
    int max = 0;
    int min = INT_MAX;
    double total = 0.0;

    printf("+-----------------+-----+\n");
    while (fscanf(entree, "%15s", nom) == 1){
      fscanf(entree, "%d", &age);
      printf("| %-15s | %3d |\n", nom, age);
      if (age > max) {max = age;}
      if (age < min) {min = age;}
      total += age;
      count += 1;

    }
    printf("+-----------------+-----+\n");
    printf("  âge minimum     : %3d\n", min);
    printf("  âge maximum     : %3d\n", max);
    printf("%2d personnes, âge moyen : %3g ans\n", count, total/count);

    fclose(entree);
  }
  return 0;

}
