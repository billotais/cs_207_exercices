#include <stdio.h>
#include <string.h>
#include <math.h>

#define TAILLE 1025

int main(void) {

  FILE* entree;
  char nom_fichier[FILENAME_MAX+1] = "";
  int taille_lue;
  int code;

 /* Le fichier à lire est donné par l'utilisateur */
  do {
    printf("Quel fichier voulez vous lire ?\n");
    fgets(nom_fichier, FILENAME_MAX+1, stdin);
    taille_lue = strlen(nom_fichier) - 1;
    if ((taille_lue >= 0) && (nom_fichier[taille_lue] == '\n'))
      nom_fichier[taille_lue] = '\0';
  } while ((taille_lue < 1) && !feof(stdin));

  if (nom_fichier[0] == '\0') return 1;

  entree = fopen(nom_fichier, "rb");
  if (entree == NULL) {
      fprintf(stderr,
              "Erreur : je ne peux pas ouvrir le fichier %s en lecture.\n",
              nom_fichier);
      return 1;
  }

  while ( ! feof(entree) ){
    taille_lue = fread(&code, sizeof(int), 1, entree);

    if (taille_lue == 1) {
      /* on transforme code en double avant de prendre sa racine carrée,
       * puis on transforme la racine carrée en char avant de l'afficher */
      printf("%c", (char) sqrt((double) code));
    }
  }
  printf("\n");
  fclose(entree);

  return 0;
}
