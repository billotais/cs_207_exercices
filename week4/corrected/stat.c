#include <stdio.h>
#include <string.h>

/* ===== CONSTANTES ===== */

/* nombre maximum de demandes en cas d'erreur */
#define NB_DEMANDES 3

/* taille maximum d'une Statistique : au plus 256 car il n'y a pas plus
   que 256 char. */
#define TAILLE 256 

/* bornes sur les caract�res � prendre en compte */
unsigned char start =  32;
unsigned char stop  = 253;


/* ===== DEFINITIONS DE TYPES ====== */

typedef unsigned long int Statistique[TAILLE];


/* ===== FONCTIONS ====== */
FILE* demander_fichier();

void initialise_statistique(Statistique a_initialiser);
/* Rappel : les tableaux sont toujours pass�s par r�f�rence. Pas
   besoin de pointeur suppl�mentaire ici */

unsigned long int collecte_statistique(Statistique a_remplir, 
                                       FILE* fichier_a_lire);

void affiche(Statistique a_afficher, unsigned long int total,
             unsigned short int taille);

/* ====================================================================== */
int main(void)
{
  FILE* fichier = demander_fichier();
  if (fichier == NULL) {
    printf("=> j'abandonne !\n");
    return 1;
  } else {
    Statistique stat;
    initialise_statistique(stat);
    affiche(stat, collecte_statistique(stat, fichier), stop - start + 1);
    fclose(fichier);
  }

  return 0;
}

/* ======================================================================
 * Fonction demander_fichier
 * ----------------------------------------------------------------------
 * In:   Un fichier (par r�f�rence) � ouvrir.
 * Out:  Ouvert ou non ?
 * What: Demande � l'utilisateur (au plus NB_DEMANDES fois) un nom de fichier
 *       et essaye de l'ouvrir en lecture.
 * ====================================================================== */
FILE* demander_fichier()
{
  FILE* f = NULL;
  char nom_fichier[FILENAME_MAX+1];
  size_t taille_lue = 0;
  unsigned short int nb = 0;

  do {
    ++nb;

    /* demande le nom du fichier */
    do {
      printf("Nom du fichier � lire : "); fflush(stdout);
      fgets(nom_fichier, FILENAME_MAX+1, stdin);
      taille_lue = strlen(nom_fichier);
      if ((taille_lue >= 1) && (nom_fichier[taille_lue-1] == '\n'))
        nom_fichier[--taille_lue] = '\0';
    } while ((taille_lue == 0) && !feof(stdin));

    if (nom_fichier[0] == '\0') {
      return NULL;
    }

    /* essaye d'ouvrir le fichier */
    f = fopen(nom_fichier, "r");

    /* est-ce que �a a march� ? */
    if (f == NULL) {
      printf("-> ERREUR, je ne peux pas lire le fichier \"%s\"\n",
             nom_fichier);
    } else {
      printf("-> OK, fichier \"%s\" ouvert pour lecture.",
             nom_fichier);
    }
  } while ((f == NULL) && (nb < NB_DEMANDES));
  
  /* la valeur de retour est le r�sultat du test entre (): 0 ou 1 */
  return f;
}


/* ======================================================================
 * Fonction initialiser_statistique
 * ----------------------------------------------------------------------
 * In:   Une Statistique � initialiser.
 * What: Initialiser tous les �l�ments d'une Statistique � z�ro.
 * ====================================================================== */
void initialise_statistique(Statistique stat)
{
  int i;
  for (i = 0; i < TAILLE; ++i) {
    stat[i] = 0;
  }
}

/* ======================================================================
 * Fonction collecte_statistique
 * ----------------------------------------------------------------------
 * In:   Une Statistique � remplir et le fichier � lire.
 * Out:  Le nombre d'�l�ments compt�s dans la Statistique.
 * What: Lit tous les caract�res dans le fichier et compte dans la Statistique
 *       combien de fois chaque caract�re apparait dans le fichier.
 * ====================================================================== */
unsigned long int collecte_statistique(Statistique stat, FILE* f)
{
  int c;                    /* le caract�re lu             */
  unsigned long int nb = 0; /* le nombre d'�l�ments compt�s */

  while ((c = getc(f)) != EOF) {
    /* est-ce que le caract�re lu est dans l'intervalle qu'on �tudie ? */
    if (( ((unsigned char) c) >= start) &&
        ( ((unsigned char) c) <= stop ) ) {
      ++(stat[c - start]); /* on incr�ment la statistique pour ce caract�re */
      ++nb; /* on incr�mente le nombre total d'�l�ments compt�s */
    }
  }

  return nb;
}

/* ======================================================================
 * Fonction affiche
 * ----------------------------------------------------------------------
 * In:   La Statistique � afficher, le nombre par rapport auquel on affiche
 *       les pourcentages (si 0 recalcule ce nombre comme la somme des
 *       �l�ments) et la taille du tableau.
 * What: Affiche tous les �l�ments d'une Statistique (valeurs absolue et
 *       relative).
 * ====================================================================== */
void affiche(Statistique stat, unsigned long int nb, unsigned short int taille)
{
  unsigned short int i;

  if (nb == 0) { /* on doit calculer la somme si nb == 0 */
    for (i = 0; i < taille; ++i)
      nb += stat[i];
  }
  
  printf("STATISTIQUES :\n");
  for (i = 0; i < taille; ++i) {
    /* on n'affiche que les r�sultats pour des statistiques sup�rieures � 0 */
    if (stat[i] != 0) {
      printf("%c : %10lu - %6.2f%%\n", (char) (i+start), stat[i],
             100.0 * stat[i] / (double) nb);
    }
  }
}
