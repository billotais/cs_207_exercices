#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define TAILLE 1025

void demander_chaine(char* reponse, int taille)
{
  int taille_lue;

  do {
    fgets(reponse, taille, stdin);
    taille_lue = strlen(reponse) - 1;
    if ((taille_lue >= 0) && (reponse[taille_lue] == '\n'))
      reponse[taille_lue] = '\0';
  } while ((taille_lue < 1) && !feof(stdin));
}

int main(void) {

  char nom_fichier[FILENAME_MAX+1];
  char phrase[TAILLE];

  puts("Dans quel fichier voulez vous �crire ?\n");
  demander_chaine(nom_fichier, FILENAME_MAX+1);
  if (nom_fichier[0] == '\0') return 1;

  printf("Entrez une phrase (< %d caract�res) :\n", TAILLE-1);
  demander_chaine(phrase, TAILLE);

  if (phrase[0] != '\0') {
    /* �criture et codage */
    int i;
    int taille;
    FILE* sortie;
    int ecrits;
    unsigned int code;

    /* On va �crire du binaire */
    sortie = fopen(nom_fichier, "wb");
    if (sortie == NULL) {
      fprintf(stderr,
              "Erreur : je ne peux pas ouvrir le fichier %s en �criture.\n",
              nom_fichier);
      return 1;
    }

    taille = strlen(phrase);
    for (i = 0; i < taille; ++i) {
      code = (unsigned char) phrase[i];
      code *= code;
      printf("%c -> %u -> %d\n", phrase[i], (unsigned char) phrase[i], code);
      ecrits = fwrite(&code, sizeof(int), 1, sortie);

      if (ecrits != 1)  {
        fprintf(stderr,
                "Erreur : je n'ai pas pu �crire plus que %d entiers (sur %d) !\n",
                i, taille);
        return 3;
      }
    }

    fclose(sortie);
  }

  return 0;
}
