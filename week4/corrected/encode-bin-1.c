#include <stdio.h>
#include <math.h>

int main(void) {
  FILE* entree;
  char nom_fichier[] = "./a_lire.bin";
  int taille_lue;
  int code;

  /* Ouverture du fichier binaire en lecture */
  entree = fopen(nom_fichier, "rb");
  if (entree == NULL) {
      fprintf(stderr,
              "Erreur : je ne peux pas ouvrir le fichier %s en lecture.\n",
              nom_fichier);
      return 1;
  }

  while (!feof(entree)) {
    /* Pour lire un fichier binaire, on utilise fread.
     * Ici, on va lire un int et le sauver dans code. 
     */
    taille_lue = fread(&code, sizeof(int), 1, entree);

    if (taille_lue == 1) {
      printf("%d -> %c\n", code, (char) sqrt((double) code));
    }
  }

  fclose(entree);
  return 0;
}
