#include <stdio.h>
#include <string.h>

/* on affiche les noms sur 15 caract�res, comme sp�cifi� dans la donn�e */
#define TAILLE_NOM 15

int main(void)
{
  char const nom_fichier[] = "data.dat"; /* le nom du fichier */
  FILE* entree;
  int taille_lue;

  char nom[TAILLE_NOM+1]; /* la donn�e "nom" � lire depuis le fichier */
  unsigned int age;       /* la donn�e "age" � lire depuis le fichier */

  /* variables n�cessaires aux diff�rents calculs */
  unsigned int nb = 0; 
  unsigned int age_max = 0; 
  unsigned int age_min = (unsigned int) -1; /* truc : -1 sera toujours le plus
                                               grand nombre repr�sentable */
  double total = 0.0;

  /* ouverture du fichier en lecture (r=read) */
  entree = fopen(nom_fichier, "r");
  
  /* on teste si l'ouverture du flot s'est bien r�alis�e */
  if (entree == NULL) {
    fprintf(stderr,
            "Erreur: le fichier %s ne peut etre ouvert en lecture !\n",
            nom_fichier);
    return 1; /* retourne autre chose que 0 car �a s'est mal pass� */
  }

  /* On commence par l'affichage du cadre */
  printf("+-----------------+-----+\n");

  /*
   * Et on boucle directement sur la condition de lecture correcte
   * du couple <nom,age> (en fait, sur la condition de lecture correcte
   * de 'age', mais comme il n'est pas possible de lire 'age' si la
   * lecture de 'nom' � �chou�...)
   */

  do {
    taille_lue = fscanf(entree, "%15s %u", nom, &age);

    if (taille_lue == 2) { /* la lecture s'est bien pass�e */
      ++nb; /* nombre de personnes + 1 */
      total += age; /* pour faire la moyenne plus tard */
      /* on v�rifie si l'�ge lu est le plus grand/petit lu jusqu'ici  */
      if (age_min > age) age_min = age; /* */
      if (age_max < age) age_max = age; /**/

      /* Affichage */
      /* le signe "-" permet d'aligner � gauche */
      printf("| %-15s | %3d |\n", nom, age);
    }
  } while (! feof(entree));

  /* Partie finale */

  fclose(entree);  /* ne pas oublier de fermer le  fichier ! */

  printf("+-----------------+-----+\n");
  printf("  �ge minimum     : %3d\n", age_min);
  printf("  �ge maximal     : %3d\n", age_max);

  printf("%d personnes, �ge moyen : %4.1f ans\n", nb, total/nb);
  /* l'�ge moyen est sur 4 chiffres dont un chiffre apr�s la virgule */

  return 0;
}
