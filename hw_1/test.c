#include <stdio.h>
#define ERR_IO     -1
#define ERR_MALLOC -2
#define ERR_NULL   -3

#define MAX_CHARS 128
typedef struct {
  unsigned char family[MAX_CHARS];
  //size_t family_size;
  unsigned char given[MAX_CHARS];
  //size_t given_size;
} name_t;

typedef struct {
  unsigned short int country_code;
  unsigned short int area_code;
  unsigned long int number;
} phone_t;

typedef struct {
  name_t name;
  phone_t phone;
} contact_t;

typedef struct {
  contact_t* contacts;
  contact_t** indices;
  unsigned int NEnt;

} ypages_t;
int print_contact(contact_t contact, FILE* file) {

  int writen = fprintf(file, "%s\n%s\n%hu %hu %lu", contact.name.family, contact.name.given, contact.phone.country_code, contact.phone.area_code, contact.phone.number);
  if (writen <= 0) {
    return ERR_IO;
  }
  else {
    return 1;
  }
}
int main (void) {
  FILE* file = fopen("test_write.dat", "a");
  name_t name = {"BILAT", "LOIS"};
  phone_t phone = {12, 34, 2302746};
  contact_t contact = {name, phone};
  print_contact(contact, file);
  return 0;
}
