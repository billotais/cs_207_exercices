
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define ERR_IO     -1
#define ERR_MALLOC -2
#define ERR_NULL   -3
#define MAX_CHARS 128

typedef struct {
  char family[MAX_CHARS];
  char given[MAX_CHARS];
} name_t;

typedef struct {
  unsigned short int country_code;
  unsigned short int area_code;
  unsigned long int number;
} phone_t;

typedef struct {
  name_t name;
  phone_t phone;
} contact_t;

typedef struct {
  contact_t* contacts;
  contact_t** indices;
  unsigned int NEnt;


} ypages_t;
int ct_compare(contact_t c1, contact_t c2) {
  // Start by comparing family names
  int nameComp = strcmp(c1.name.family, c2.name.family);
  // They are different, return comparaison result
  if (nameComp != 0) return nameComp > 0 ? 1 : -1;
  // If same family name, compare first names
  else return strcmp(c1.name.given, c2.name.given) > 0 ? 1 : -1;

}
int is_prefix(unsigned char s1[], unsigned char s2[], size_t n) {
  return (strncmp(s1, s2, n) == 0 ? 1 : 0);
}
void ct_swap(contact_t** id_1, contact_t** id_2) {

  contact_t* prov = *id_1;
  *id_1 = *id_2;
  *id_2 = prov;
}
int print_contact(contact_t contact, FILE* file) {
  // Try to print contact
  int writen = fprintf(file, "%s\n%s\n%04hu %04hu %06lu\n", contact.name.family, contact.name.given, contact.phone.country_code, contact.phone.area_code, contact.phone.number);
  // If failed return ERR_IO
  if (writen < 0) return ERR_IO;
  else return 0;
}
int print_ypages(ypages_t* pages, const char* const name) {
  // If pointer to ypages is empty, return ERR_NULL
  if (pages == NULL) return ERR_NULL;
  // Try to open output file
  FILE* f = fopen(name, "w");
  // ERR_IO if failed
  if (f == NULL) return ERR_IO;
  else {
    // Print number of contacts
    fprintf(f,"%d\n", pages->NEnt);
    // For each contact, try to call print_contact
    for (int i = 0; i < pages->NEnt; ++i) {
      int cont_print = print_contact(*pages->indices[i], f);
      if (cont_print != 0) return cont_print;

    }
    // Close the file
    fclose(f);
    return 0;
  }

}
int read_ypages(ypages_t* pages, const char* const name) {
  // If pointer to ypages is empty, return ERR_NULL
  if (pages == NULL) return ERR_NULL;
  // Try to open input file
  FILE* f = fopen(name, "r");
  if (f == NULL) return ERR_IO;

  else {

    // Read the number of contacts
    int nbrContacts = 0;
    fscanf(f, "%d", &nbrContacts);

    // Not a valid input file -> ERR_IO, else save contacts number
    if (nbrContacts < 0) return ERR_IO;
    pages->NEnt = nbrContacts;
    // Allocate tables to store contacts
    pages->contacts = calloc(nbrContacts,sizeof(contact_t));
    pages->indices = calloc(nbrContacts,sizeof(contact_t*));

    // If we coulnd'nt allocate both, return ERR_MALLOC
    if (pages->indices == NULL || pages->contacts == NULL)  return ERR_MALLOC;

    // For each contact
    for (int i = 0; i < nbrContacts; ++i) {
      // Scan Family / Given, if we couldn't read them, ERR_IO
      fscanf(f, "%s\n", pages->contacts[i].name.family);
      if (pages->contacts[i].name.family == NULL) return ERR_IO;
      fscanf(f, "%s\n", pages->contacts[i].name.given);
      if (pages->contacts[i].name.given == NULL) return ERR_IO;
      // Scan 3 parts of phone number, if we can't, error
      int tryReadPhone1 = fscanf(f, "%hu", &(pages->contacts[i].phone.country_code));
      if (tryReadPhone1 < 0) return ERR_IO;
      int tryReadPhone2 = fscanf(f, "%hu", &(pages->contacts[i].phone.area_code));
      if (tryReadPhone2  < 0) return ERR_IO;
      int tryReadPhone3 = fscanf(f, "%lu\n", &(pages->contacts[i].phone.number));
      if (tryReadPhone3 < 0) return ERR_IO;

      // Create a link betweeen indices and contacts
      (pages->indices[i]) = &(pages->contacts[i]);
    }

  }
  // Close file
  fclose(f);
  return 0;
}
void free_ypages(ypages_t* pages) {
  // Free both allocated "arrays"
  free(pages->contacts);
  free(pages->indices);
}
void heapify(contact_t** A, size_t i, size_t size) {

  // If both children are in the array
  if (i*2 + 2 < size) {
    // If one of them is bigger
    if (ct_compare(*A[i],*A[2*i + 1]) < 0 || ct_compare(*A[i], *A[2*i + 2]) < 0) {
      // If first child bigger
      if (ct_compare(*A[2*i + 1], *A[2*i + 2]) > 0) {
        // Exchange the contacts, call heapify on the new child
        ct_swap(A + i, A + 2*i + 1);
        heapify(A, 2*i + 1, size);
      }
      // If second child bigger
      else {
        // Exchange the contacts, call heapify on the new child
        ct_swap(A + i, A + 2*i + 2);
        heapify(A, 2*i + 2, size);
      }
    }
  }
  // If only the first child is in the array (-> no second child)
  else if (i*2 + 1 < size) {
    // If the child is bigger
    if (ct_compare(*(A[i]), *(A[2*i + 1])) < 0) {
      // Exchange it
      ct_swap(A + i, A + 2*i + 1);
      heapify(A, 2*i + 1, size);
    }
  }
}
void buildheap(contact_t** A, size_t size) {
  // Simply follow the algorithm
  for (int i = (size-1) / 2; i >= 0; --i){
    heapify(A, i, size);
  }
}
void heapsort(ypages_t* pages) {
  // Simply follow the algorithm
  contact_t** A = pages->indices;
  size_t size = pages->NEnt;
  buildheap(A, size);
  for (size_t i = size - 1; i >= 1; --i) {
    ct_swap(A, A + i);
    heapify(A, 0, i);
  }
}
unsigned int query(ypages_t* pages, char* qkey, FILE* qout){

  // Initalize all needed variables

  int found = 0;
  int sizeRead = strlen(qkey);
  int start = 0;
  int end = pages->NEnt - 1;
  int mid;

  // While we havn't found any contact and the comparaison length if not 0
  while (found == 0 && sizeRead > 0) {
    // Find the middle of the segment og the array
    mid = (end + start)/2;
    // Compare the middle elements quith the querry word
    int cmp = strncmp(qkey, pages->indices[mid]->name.family, sizeRead);
    // If our word is smaller than the middle element, search in the left side only
    if (cmp < 0) end = mid - 1;
    // If our word is smaller than the middle element, search in the right side only
    else if (cmp > 0)  start = mid + 1;
    // If the middle element correponds to the querry
    else {

      int start_mid = mid;
      int end_mid = mid;

      int i = mid - 1;
      // While we have correponding elements on the left, move starting point
      while (i >= start && is_prefix(qkey, pages->indices[i]->name.family, sizeRead) == 1) start_mid = i--;

      i = mid + 1;
      // While we have correponding elements on the right, move ending point
      while (i <= end && is_prefix(qkey, pages->indices[i]->name.family, sizeRead) == 1) end_mid = i++;

      // Go through all contacts between starting and ending points
      for (int j = start_mid; j <= end_mid; ++j) {
        // Print them
        int p = print_contact(*pages->indices[j], qout);
        if (p < 0) fprintf(stderr, "Error while writing contact during querry\n");
        ++found;

      }
    }
    // If we havn't found any contact and the array is completely reduced, decrease comparing sizeRead
    // and restore limits
    if (found == 0 && start > end) {
      sizeRead--;
      start = 0;
      end = pages->NEnt-1;
    }
  }
  // Return number of found contacts
  return found;
}
int main()
{
    const char* const dat_fname     = "data_large.dat"     ;
    const char* const replica_fname = "data_large.replica" ;
    const char* const sorted_fname  = "data_large.sorted"  ;
    const char* const query_fname   = "query_large.dat"    ;
    const char* const result_fname  = "query_large.ans"    ;

    ypages_t yp = { NULL, NULL, 0 };
    int err = 0;

    /* Read the yellow pages register */
    err = read_ypages(&yp, dat_fname);
    if (err != 0) {

        fprintf(stderr, "Cannot read %s: %d\n", dat_fname, err);
        return ERR_IO;

    } else {

        /* Replicate the yellow pages register */
        err = print_ypages(&yp, replica_fname);
        if (err != 0) {
            fprintf(stderr, "Cannot write %s: %d\n", replica_fname, err);
            return ERR_IO;
        }

        /* Sort the yellow pages register */
        heapsort(&yp);

        /* Print out the sorted the yellow pages register */
        err = print_ypages(&yp, sorted_fname);
        if (err != 0) {
            fprintf(stderr, "Cannot write %s: %d\n", sorted_fname, err);
            return ERR_IO;
        }

        /* Open the query files */
        FILE* qin = fopen(query_fname, "r");
        if (NULL == qin) {
            fprintf(stderr, "[ERROR]: Cannot open 'query.dat' for reading (code %d)\n", ERR_IO);
            return ERR_IO;
        }

        FILE* qout = fopen(result_fname, "w");
        if (NULL == qout) {
            fprintf(stderr, "[ERROR]: Cannot open 'query.ans' for writing (code %d)\n", ERR_IO);
            fclose(qin);
            return ERR_IO;
        }

        /* Read the total number of queries */
        size_t nqueries = 0;
        err = fscanf(qin, "%zu", &nqueries);
        if (err != 1) {
            fprintf(stderr, "[ERROR]: Cannot read from 'query.dat' (code %d)\n", ERR_IO);
            fclose(qin);
            fclose(qout);
            return ERR_IO;
        }

        /* Iterate over all queries in 'query.dat' */
        for (size_t i = 0; i < nqueries; ++i) {

            /* Query key buffer */
            char qkey[MAX_CHARS];
            memset(qkey, 0, sizeof(qkey));

            /* Read one query key */
            err = fscanf(qin, " %s", qkey);
            if (err != 1) {
                fprintf(stderr, "[ERROR]: Cannot read from 'query.dat' (code %d)\n", ERR_IO);
                fclose(qin);
                fclose(qout);
                return ERR_IO;
            }

            /* Answer the query */
            unsigned int found = query(&yp, qkey, qout); // splited into two to ensure proper output order (query has side-effects on qout)
            fprintf(qout, "%u contacts found\n", found);
            fprintf(qout, "=====\n");
        }

        /* Close I/O files */
        fclose(qin);
        fclose(qout);
    }

    /* Free all allocated memory */
    free_ypages(&yp);

    /* Execution ended successfully */
    return 0;
}
