// C99
#include <stdio.h>

typedef unsigned char octet;

// ======================================================================
static inline void affiche_bit(const octet c,
                               const octet position_pattern)
{
  putchar(c & position_pattern ? '1' : '0');
}

// ======================================================================
void affiche_binaire(const octet c) {
  for(octet mask = 0x80; mask; mask >>= 1)
    affiche_bit(c, mask);
}

// ======================================================================
void dump_mem(const octet* ptr, size_t length)
{
  const octet* const end = ptr + length;
  for (const octet* p = ptr; p < end; ++p) {
    printf("%p : ", p);
    affiche_binaire(*p);
    printf(" %3u ", (unsigned int) *p);
    if ((*p >= 32) && (*p <= 126)) {
      printf("'%c'", *p);
    }
    putchar('\n');
  }
  puts("----------------------------------------");
}

// ======================================================================
int main(void)
{
  int a = 64 + 16;
  int b = -a;
  double x = 0.5;
  double y = 0.1;

  dump_mem( (octet*) &a, sizeof(a) );
  dump_mem( (octet*) &b, sizeof(b) );
  dump_mem( (octet*) &x, sizeof(x) );
  dump_mem( (octet*) &y, sizeof(y) );

  return 0;
}
