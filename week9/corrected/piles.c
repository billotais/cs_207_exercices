#include <stdio.h>
#include <string.h>

typedef char type_el;

/* version simpliste avec tableau de taille fixe.
   Une version plus réaliste utiliserait des tableaux dynamique.
*/

#define STACK_OVERFLOW 256
#define VIDE -1
typedef struct {
  type_el tab[STACK_OVERFLOW];
  int tete;
} Pile;

/* Prototypes des fonctions */
int empile(Pile* p, type_el e);
void depile(Pile* p);
type_el top(Pile* p);
int est_vide(Pile* p);
void init_Pile(Pile* p);

/* vérification de parenthèsage */
int check(char* parentheses);

/* -------------------------------------------------------------------- */
#define MAX 1024
int main(void)
{
  char s[MAX+1];
  int taille_lue;

  do {
    printf("Entrez une expresssion parenthèsée : ");
    fgets(s, MAX, stdin);
    taille_lue = strlen(s) - 1;
    if ((taille_lue >= 0) && (s[taille_lue] == '\n'))
      s[taille_lue] = '\0';
    if (s[0] != '\0')  /* pas vide */
      printf(" -> %s\n", check(s) ? "OK" : "Erreur");
  } while ((taille_lue < 1) && !feof(stdin));

  return 0;
}

/* -------------------------------------------------------------------- */
int empile (Pile* p, type_el e)
{
  ++(p->tete);
  if (p->tete >= STACK_OVERFLOW) {
    p->tete = STACK_OVERFLOW;
    return 0; }
  else {
    p->tab[p->tete] = e;
  }
  return 1;
}

/* ---------------------------------------------------------------------- */
void depile (Pile* p)
{
  if (!est_vide(p)) --(p->tete);
}

/* ---------------------------------------------------------------------- */
type_el top (Pile* p)
{
  if (!est_vide(p))
    return p->tab[p->tete];

  else /* que faire ??  -> totalement arbitraire... */
    return 0;
}

/* ---------------------------------------------------------------------- */
int est_vide(Pile* p)
{
  return ((p->tete < 0) || (p->tete >= STACK_OVERFLOW));
}

/* ---------------------------------------------------------------------- */
void init_Pile(Pile* p)
{
  p->tete = -1; /* convention => pile vide */
}

/* ---------------------------------------------------------------------- */
int check(char* s) {
  Pile p;
  unsigned int i;

  init_Pile(&p);

  for (i = 0; i < strlen(s); ++i) {
    if ((s[i] == '(') || (s[i] == '['))
      empile(&p, s[i]);
    else if (s[i] == ')') {
      if ((!est_vide(&p)) && (top(&p) == '('))
	depile(&p); 
      else 
	return 0;
    } else if (s[i] == ']') {
      if ((!est_vide(&p)) && (top(&p) == '['))
	depile(&p); 
      else 
	return 0;
    }
  }

  return est_vide(&p);
}
  
