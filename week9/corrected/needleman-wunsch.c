// C99

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <stdint.h> /* pour SIZE_MAX */
#ifndef SIZE_MAX
#define SIZE_MAX (~(size_t)0)
#endif

/* On repr�sente les pr�decesseurs par le d�placement relatif */
typedef enum {
	DirDiag, /* en diagonale */
	DirHorz, /* horizontalement */
	DirVert	 /* verticalement */
} Dir;

/* Une cellule du tableau */
typedef struct {
	int val; /* la "vraie" valeur � stocker  */
	Dir dir; /* la direction du pr�d�cesseur */
} Cell;

/* Table rectangulaire des valeurs */
typedef struct {
	size_t width;  /* largeur */
	size_t height; /* hauteur */
	Cell*  tab;    /* tableau */
} Table;

/* Solution : ici une solution est un tableau dynamique de transformations.
 * Dans le cas des pointeurs ce pourrait �tre une liste cha�n�e par exemple */
typedef struct {
	size_t size;  /* la taille du tableau      */
	Dir* dirs;    /* la suite des d�placements */
} Solution;


Table* computeTable(const char* s1, const char* s2);
Solution* extractSolution(const Table* tab);
void showSolution(Solution* sol, const char* s1, const char* s2);
void freeTable(Table* tab);

Table* computeTable(const char* s1, const char* s2) {
	size_t i,j;

	Table* res = malloc(sizeof(Table));
	if (res==NULL) return NULL;

	res->width  = strlen(s1) + 1; /* s1 horizontalement */
	res->height = strlen(s2) + 1; /* s2 verticalement */
    if (SIZE_MAX / res->height < res->width) { free(res); return NULL; }
	res->tab = calloc(res->height * res->width, sizeof(Cell));
	if (res->tab==NULL) { free(res); return NULL; }

	for (i = 0; i < res->height; ++i) {
        Cell* c = res->tab + i*res->width; // j = 0
        c->val = 0;
        c->dir = DirVert; 
    }
	for (j = 0; j <res->width; ++j) { 
        Cell* c = res->tab + j; // i = 0
        c->val = 0;
        c->dir = DirHorz;
    }
	for (i = 1; i < res->height; ++i) {
		for (j = 1; j < res->width; ++j) {
            Cell* c = res->tab + i*res->width + j;

			int s = (s1[j-1] == s2[i-1]) ? 2 : -1;
			int diag = res->tab[ (i-1)*res->width + j-1 ].val + s ; 
            int horz = res->tab[ (i  )*res->width + j-1 ].val - 2 ;
            int vert = res->tab[ (i-1)*res->width + j   ].val - 2 ;
			
			if (diag>horz && diag>vert) {
				c->val = diag;
				c->dir = DirDiag;
			} else if (horz>vert) {
				c->val = horz;
				c->dir = DirHorz;
			} else {
				c->val = vert;
				c->dir = DirVert;
			}
		}
	}
	return res;
}

Solution* extractSolution(const Table* tab) {
	size_t i,j;

	Solution *sol = malloc(sizeof(Solution));
	if (sol==NULL) return NULL;

	sol->size = 0;
	sol->dirs = calloc(tab->width + tab->height, sizeof(Dir));
	if (sol->dirs==NULL) return NULL;

	i = tab->height ? (tab->height - 1) : 0;
	j = tab->width  ? (tab->width  - 1) : 0;

	while(i>0 || j>0) {
		Dir dir = tab->tab[i*tab->width+j].dir;
		sol->dirs[sol->size]=dir;
		sol->size++;

		switch (dir) {
			case DirHorz: --j;      break;
			case DirVert: --i;      break;
			case DirDiag: --i; --j; break;
		}
	}
	
	/* Inversion pour remettre les mouvements dans le bon ordre */
	for (i=0; i<sol->size/2; ++i) {
		Dir tmp = sol->dirs[i];
		sol->dirs[i] = sol->dirs[sol->size-1-i];
		sol->dirs[sol->size-1-i] = tmp;
	}

	/* R�duction de la solution � la bonne taille */
	sol->dirs = realloc(sol->dirs, sol->size*sizeof(Dir));
	
	return sol;
}

void showSolution(Solution* sol, const char* s1, const char* s2) {
	size_t i,j;
	for (i=0,j=0; i<sol->size; ++i) {
		switch (sol->dirs[i]) {
			case DirDiag: /*continued*/
			case DirHorz: printf("%c",s1[j]); ++j; break;
			case DirVert: printf("_");
		}
	}
	printf("\n");
	for (i=0,j=0; i<sol->size; ++i) {
		switch (sol->dirs[i]) {
			case DirDiag: /*continued*/
			case DirVert: printf("%c",s2[j]); ++j; break;
			case DirHorz: printf("_");
		}
	}
	printf("\n");
}

void freeTable(Table* tab) {
	free(tab->tab);
	free(tab);
}

int main(void) {
	char s1[] = "Bonjour monsieur, quelle heure est-il � votre montre ?";
	char s2[] = "Bonne journ�e madame, que l'heureuse fillette vous montre le chemin";
	
	Table* tab = computeTable(s1,s2);
	Solution* sol = extractSolution(tab);

	showSolution(sol, s1, s2);
	
	freeTable(tab);
	free(sol->dirs);
	free(sol);

	return 0;
}
